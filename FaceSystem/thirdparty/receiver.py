#socket:一组实现tcp协议的api
#目前端口为32位整数;32位整数的最大值65535
# 参考来自 https://blog.csdn.net/qq_42420425/article/details/82502708
import socket
import re
import os
def service_client(new_socket):
    #1.接收浏览器发送过来的请求，即http请求
    request = new_socket.recv(1024).decode() #1024缓存区的大小t
    print(request)
    # exit(0)
 
    #2.返回http格式的数据，给浏览器
    #2.1准备发送给浏览器 的数据——header
    response = "HTTP/1.1 200 OK\r\n"
    response+= "Content-Type:text/html;charset=utf-8\r\n"#可以把200换成404 403测试，不同错误会有不同响应
    response+="\r\n"	#u：unicode；r：防止转译,\r\n防转译换行
    #2.2准备发送给浏览器的数据body；
    response+="天气不错，你觉得呢？"
    new_socket.send(response.encode("utf-8"))
    new_socket.close()
    exit(0)
    #获取get中的文件名，查询
    # base_dir = os.path.dirname()
    # file_name = request_splite[0].split(' ')[1]
    # path_name = 'Http'+file_name	#Http包名
 
    # #打开该文件，展示在页面中
    # with open(path_name,"rb") as f:    
    #     html_content = f.read()
    #     #将response header发送给浏览器
    #     new_socket.send(response.encode("utf-8")) #utf-8会显示乱码
    #     #将response_body发送给浏览器
    #     new_socket.send(html_content)
    #     #关闭套接字
    # new_socket.close()
 
def main():
    #1.创建套接字
    tcp_server_socket = socket.socket(socket.AF_INET,socket.SOCK_STREAM)  #UDP协议：socket.SOCK_DGRAM
    #设置可选参数
    tcp_server_socket.setsockopt(socket.SOL_SOCKET,socket.SO_REUSEADDR,1)
 
    #2.绑定:  ,端口号
    tcp_server_socket.bind(("",7891))
 
    #3.变为监听套接字:队列的大小，最多支持128个连接
    tcp_server_socket.listen(128)
 
    while True:
        #4.等待新客户端的链接
        new_socket,client_addr = tcp_server_socket.accept()#阻塞 I/O 输入/输出
        # print("is run?")
        #5.为这个客户端服务
        service_client(new_socket)
 
    #关闭监听套接字
    tcp_server_socket.close()
 
#在当前文件调用方法执行
if __name__ == '__main__':
    main()
