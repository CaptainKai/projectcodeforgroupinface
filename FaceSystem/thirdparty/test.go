package main
import (
    "fmt"
    "github.com/garyburd/redigo/redis"
	// "github.com/go-redis/redis"

	// "context"

)

var rdb *redis.Client
 
func initRedis() (err error) {
    ctx,cancel := context.WithTimeout(context.Background(),time.Second*10)
    defer cancel()
    rdb = redis.NewClient(&redis.Options{
        Addr: "bba.loiot.com",
        Password: "loit2021",
        PoolSize: 200,
		Db: 3,
		Port: 6397,
    })
    _,err = rdb.Ping(ctx).Result()
    if err !=nil{
        fmt.Println("ping redis failed err:",err)
        return err
    }
    return nil
}
 
func main() {
    err := initRedis()
    if err !=nil{
        fmt.Println("init redis failed err :",err)
        return
    }
    ctx := context.Background()
 
    var cursor uint64
    keys,cursor,err := rdb.Scan(ctx,cursor,"*",100).Result()
    if err !=nil{
        fmt.Println("scan keys failed err:",err)
        return
    }
    for _,key := range keys{
        //fmt.Println("key:",key)
        sType,err := rdb.Type(ctx,key).Result()
        if err !=nil{
            fmt.Println("get type failed :",err)
            return
        }
        fmt.Printf("key :%v ,type is %v\n",key,sType)
        if sType == "string" {
            val,err := rdb.Get(ctx,key).Result()
            if err != nil{
                fmt.Println("get key values failed err:",err)
                return
            }
            fmt.Printf("key :%v ,value :%v\n",key,val)
        }else if sType == "list"{
            val,err := rdb.LPop(ctx,key).Result()
            if err !=nil{
                fmt.Println("get list value failed :",err)
                return
            }
            fmt.Printf("key:%v value:%v\n",key,val)
        }
    }
}