import functools
import logging
from collections import OrderedDict

from django.utils.decorators import method_decorator
from django.views.decorators.csrf import csrf_exempt

from rest_framework.views import APIView as View
from rest_framework.response import Response
from rest_framework.status import *

import datetime


def response(data, status_code):
    return Response(data, status=status_code, headers={"Access-Control-Allow-Origin": "*"})


class APIView(View):

    def success(self, data=None):
        # print(data)
        return response({"error": None, "data": data}, HTTP_200_OK)

    def error(self, msg="error", err="error", status_code=HTTP_400_BAD_REQUEST):
        return response({"error": err, "data": msg}, status_code)

    def _serializer_error_to_str(self, errors):
        for k, v in errors.items():
            if isinstance(v, list):
                return k, v[0]
            elif isinstance(v, OrderedDict):
                for _k, _v in v.items():
                    return self._serializer_error_to_str({_k: _v})

    def invalid_serializer(self, serializer):
        k, v = self._serializer_error_to_str(serializer.errors)
        if k != "non_field_errors":
            return self.error(err="invalid-" + k, msg=k + ": " + v, status_code=HTTP_400_BAD_REQUEST)
        else:
            return self.error(err="invalid-field", msg=v, status_code=HTTP_400_BAD_REQUEST)

    def server_error(self):
        return self.error(err="server-error", msg="server error", status_code=HTTP_500_INTERNAL_SERVER_ERROR)


def tid_maker():
    return '{0:%Y%m%d%H%M%S%f}'.format(datetime.datetime.now())


def get_mac_address():
    '''
    @summary: return the MAC address of the computer
    '''
    import sys
    import os
    mac = None
    if sys.platform == "win32":
        for line in os.popen("ipconfig /all"):
            print(line)
            if line.lstrip().startswith("Physical Address"):
                mac = line.split(":")[1].strip().replace("-", ":")
                break
    else:
        for line in os.popen("/sbin/ifconfig"):
            if 'Ether' in line:
                mac = line.split()[4]
                break
            if 'ether' in line:
                mac = line.split()[1]
                break
    return mac


from random import Random  # 用于生成随机码


# 生成随机字符串
def random_str(randomlength=8):
    str = ''
    chars = 'AaBbCcDdEeFfGgHhIiJjKkLlMmNnOoPpQqRrSsTtUuVvWwXxYyZz0123456789'
    length = len(chars) - 1
    random = Random()
    for i in range(randomlength):
        str += chars[random.randint(0, length)]
    return str


import base64
import cv2


def img2base64(img):
    return base64.b64encode(cv2.imencode('.jpg', img)[1]).decode()

_local_ip = None

import socket
def get_host_ip():

    global _local_ip
    s = None
    try:
        if not _local_ip:
            s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
            s.connect(('8.8.8.8', 80))
            _local_ip = s.getsockname()[0]
        return _local_ip
    finally:
        if s:
            s.close()


import uuid
def getmac():
    mac = uuid.UUID(int = uuid.getnode()).hex[-12:]
    return mac

def sys_validation():
    # start_date = datetime.date(2021,11,27)
    get_host_ip()
    assert _local_ip is not None
    mac = getmac()
    if _local_ip!="10.0.120.162":
        return False
    if mac!="fa163e045870":
        return False
    # now_date = datetime.datetime.now().date()
    # if now_date.__sub__(start_date).days>=365 or now_date.__sub__(start_date).days<0:
    #     return False
    return True

# if sys_validation():
#     print("Welcome!")
# else:
#     print("Unified Code is invalidate!\nPlease contact USTB@PRIR manager: m13121032012@163.com!")
#     exit(0)


import numpy as np

STD = [39.2946, 69.5318, 62.0252, 29.5493, 65.7200, \
       21.6963, 14.5014, 56.7366, 87.3655, 80.2041]
UNIT_SIZE = 112


def findNonreflectiveSimilarity(src,dst):
    p=src
    A=np.zeros(40)
    for i in range(5):
        j =i*4
        A[j]=p[i]
        A[j+21]=p[i]*(-1)
        A[j+1]=A[j+20]=p[i+5]
        A[j+2] = A[j+23] = 1
        A[j+3] = A[j+22] = 0
    B=np.reshape(A,(10,4))
    # print(B)
    B=np.mat(B)
    A=B.I
    # print(A)
    A=np.array(A)
    M=np.dot(A,dst)

    M=np.array(M)
    m=np.zeros(6)

    m[0]=M[0]
    m[3]=M[1]*(-1)
    m[1]=M[1]
    m[2]=M[2]
    m[4]=M[0]
    m[5]=M[3]
    I=np.mat(np.reshape(m,(2,3)))

    # print(I)
    return I

import os
BASE_DIR = os.path.dirname(os.path.abspath(__file__))

from .recognition import *
recognition_model_name = "SimpleResnet_20"
recognition_model_path = BASE_DIR + "/recognition/snapshot/SimpleResnet_20_base/amsoft_33_checkpoint.pth"
face_extractor = FeatureExtractor(recognition_model_name)
face_extractor.load_model(recognition_model_path)


from .detection import *
face_detector = Face_Detector()
face_detector.load_model(BASE_DIR + "/detection/weights/landmark_211/Final_FaceBoxes.pth")
# face_detector.load_model(BASE_DIR + "/detection/weights/mask_landmark_augue/Final_FaceBoxes.pth")
detector_object = face_detector

# from .detection2 import *
# face_detector = Face_Detector()
# face_detector.load_model(BASE_DIR + "/detection2/weights/retinaface/Resnet50_Final.pth")
# # face_detector.load_model(BASE_DIR + "/detection/weights/mask_landmark_augue/Final_FaceBoxes.pth")
# detector_object = face_detector

test_file_path = BASE_DIR + "/test.jpg"

import queue
import threading
class myThread (threading.Thread):
    def __init__(self, model, q, lock, exitFlag=0):
        threading.Thread.__init__(self)
        self.model = model
        self.q = q

        self.lock = lock
        self.exitFlag = exitFlag
        self.result_list = []

        # self.recorder = Recorder()
    
    def run(self):
        self.process_data()

    def process_data(self):
        queueLock = self.lock
        workQueue = self.q
        q = self.q
        model = self.model
        while not self.exitFlag:
            queueLock.acquire()
            if not workQueue.empty():
                id,img,kwargs = q.get()

                queueLock.release()
                if kwargs is not None:
                    result = self.model.run(img, **kwargs)
                else:
                    result = self.model.run(img)
                queueLock.acquire()
                self.result_list.append([id, result])
                queueLock.release()
            else:
                queueLock.release()
                # time.sleep(1)
    
    def exit(self):
        self.exitFlag=1


class myThread_compare(threading.Thread):
    def __init__(self, features, rank_N, q, lock, exitFlag=0):
        threading.Thread.__init__(self)
        self.features = features
        self.rank_N = rank_N
        self.q = q
        self.lock = lock
        self.exitFlag = exitFlag
        self.result_list = []

        # self.recorder = Recorder()
    
    def run(self):
        self.process_data()

    def process_data(self):
        queueLock = self.lock
        workQueue = self.q
        features = self.features
        while not self.exitFlag:
            queueLock.acquire()
            if not workQueue.empty():
                key_index_start, feature_list = workQueue.get()
                queueLock.release()
                # print(key_index_start, len(feature_list))
                score = np.dot(np.array(feature_list), np.array(features).transpose())
                score = np.absolute(score)
                score_index_sorted = np.argsort(score, axis=0) # db_length*feature_num
                score_sorted = np.sort(score, axis=0)
                expected_index = score_index_sorted[-self.rank_N:,:]
                expected_score = score_sorted[-self.rank_N:,:]
                # expected_score = np.expand_dims(expected_score, axis=0).repeat(2,axis=-1)
                # expected_score.dtype=[('key',"S10"), ("value",float)]
                # expected_score = expected_score.tolist()
                # for i in range(self.rank_N):
                #     for j in range(len(features)):
                #         expected_score[i,j,0]=key_index_start+expected_index[i,j]
                expected_index = key_index_start+expected_index
                self.result_list.append([expected_score,expected_index])
                del feature_list
                del score
            else:
                queueLock.release()
                # time.sleep(1)
    
    def exit(self):
        self.exitFlag=1


class Timer(object):
    """A simple timer."""
    def __init__(self):
        self.total_time = 0.
        self.calls = 0
        self.start_time = 0.
        self.diff = 0.
        self.average_time = 0.

    def tic(self):
        # using time.time instead of time.clock because time time.clock
        # does not normalize for multithreading
        self.start_time = time.time()

    def toc(self, average=True):
        self.diff = time.time() - self.start_time
        self.total_time += self.diff
        self.calls += 1
        self.average_time = self.total_time / self.calls
        if average:
            return self.average_time
        else:
            return self.diff

    def clear(self):
        self.total_time = 0.
        self.calls = 0
        self.start_time = 0.
        self.diff = 0.
        self.average_time = 0.

import redis
# from tqdm import tqdm

class RedisClass():
    def __init__(self, host, port, \
                 password, db_index):
        self.host = host
        self.port = port
        self.password = password
        self.db_index = db_index
        self.pool = redis.ConnectionPool(host=self.host, port=self.port, password=self.password, db=self.db_index)
        self.r = redis.StrictRedis(connection_pool=self.pool)
        self.key_list = []
        self.feature_list = []
        self.get_all()

    def get_all(self):
        '''
        :return: [id, id...],[array, array]
        '''
        # print(self.r)
        pipe = self.r.pipeline()
        self.key_list = []
        key_list_temp = []
        # keys = self.r.keys()
        keys = self.r.keys()# [:200] # TODO]
        # print(keys)
        key_length=0
        for key in keys:
            pipe.lrange(key, 0, -1)
            key_list_temp.append(key)
            key_length+=1
        self.feature_list = []
        _t = Timer()
        _t.tic()
        i=0
        for (k, v) in zip(key_list_temp, pipe.execute()):
        # for i, (k，v) in tqdm(zip(key_list_temp, pipe.execute())):
            if len(v) != 512:
                print("the value for %s is invalid"%(k))
                continue
            self.key_list.append(k.decode())
            self.feature_list.append(np.array(v, dtype='float32'))
            i+=1
            if (i+1)%1000==0:
                _t.toc()
                _t.tic()
                eta = _t.diff * (key_length-i) / 1000
                print("%d/%d, time consuming %s, eta:%s"%(i+1, key_length, str(_t.diff), str(eta)))
                
        _t.toc()
        # print("%d/%d, average time consuming %s, eta:%s"%(i, key_length, str(_t.average_time), str(eta)))
        print("read %d features!"%(key_length))
        # return self.key_list, self.feature_list

    def compare(self, features, rankn=1):
        '''
        :param features: [array, array]
        :param rankn:
        :return: [[[id, score]...]]
        '''
        self.get_all()
        result = []
        if len(features) == 0:
            return result
        # print(self.key_list)

        if len(self.key_list) < rankn:
            print("there is no enough data for compare")
            for i in range(len(features)):
                result.append([])
                for j in range(rankn):
                    result[-1].append([0, 0])
            return result
        if len(self.key_list)<=10000:
            score = np.dot(np.array(self.feature_list), np.array(features).transpose())
            score = np.absolute(score)
            score_index_sorted = np.argsort(score, axis=0) # db_length*feature_num
            expected_index = score_index_sorted[-rankn:][:]
            for i in range(len(features)):
                result.append([])
                for j in range(rankn):
                    result_index = expected_index[j][i]
                    result[-1].append([self.key_list[result_index], score[result_index, i]])
            return result

    def refresh(self):
        self.get_all()


def cluster(feature_list, threshold):
    if len(feature_list)==0:
        return []
    id_list = [-1]*len(feature_list)
    id_list[-1] = 0
    id_record = 0
    score = np.dot(np.array(feature_list), np.array(feature_list).transpose())
    score = np.absolute(score)
    # expected_index = score_index_sorted[-1:][:]
    id_queue = [i for i in range(len(feature_list))]
    id_index = id_queue.pop()
    while len(id_queue)!=0:
        score_list = score[:][id_index]
        invalid_index = np.where(score_list>=threshold)
        for index in invalid_index[0]:
            if index!=id_index and id_list[index]==-1:
                id_list[index] = id_record
                id_queue.append(index)
        id_index = id_queue.pop()
        if id_list[id_index]==-1:
            id_record +=1
            id_list[id_index]=id_record
    return id_list


# TODO
class FeatureDB():
    def __init__(self, db_root, db_list):
        self.root_path = db_root
        self.list_path = db_list
        if not os.path.exists(self.list_path):
            with open(self.list_path, "w") as f:
                f.close()
        self.lock = False
        self.db = []
        self.load_db()

    def load_db(self):
        list_file = open(self.list_path, "r")
        self.list = [x.strip() for x in list_file.readlines()]
        list_file.close()
        self.lock = True
        for id in self.list:
            feature_path = os.path.join(self.root_path, id+".bin")
            feature = np.float32(np.fromfile(feature_path,dtype=np.float32))[4:]
            # feature = np.float32(struct.unpack_from())[4:]
            self.db.append(feature/cv2.norm(feature))
        self.lock = False
    
    def search(self, id):
        while True:
            if not self.lock:
                if id in self.list:
                    return True
                else:
                    return False
            print("waiting")

    def add(self, id, feature):
        if self.search(id):
            print("exists!")
            return False
        while True:
            if not self.lock:
                self.lock=True
                self.list.append(id)
                self.db.append(feature)

                dest_path = os.path.join(self.root_path, id+".bin")
                dest_file = open(dest_path,'wb')
                dest_file.write(struct.pack('4i', len(feature), 1, 4, 5))
                dest_file.write(struct.pack('%df'%len(feature), *feature))
                dest_file.close()

                list_file = open(self.list_path, "a+")
                list_file.write(id+"\n")
                list_file.close()
                self.lock=False
                return True
            print("waiting")

    def delet(self, id):
        if not self.search(id):
            return False
        while True:
            if not self.lock:
                self.lock=True
                id_index = self.list.index(id)
                self.list.remove(id)
                self.db.pop(id_index)

                dest_path = os.path.join(self.root_path, id+".bin")
                os.remove(dest_path)

                list_file = open(self.list_path, "w")
                for id in self.list:
                    list_file.write(id+"\n")
                list_file.close()

                self.lock=False
                return True
            print("waiting")

    def change(self, id, feature):
        if not self.search(id):
            return False
        while True:
            if not self.lock:
                self.lock=True
                id_index = self.list.index(id)
                self.db[id_index]=feature

                dest_path = os.path.join(self.root_path, id+".bin")
                dest_file = open(dest_path,'wb')
                dest_file.write(struct.pack('4i', len(feature), 1, 4, 5))
                dest_file.write(struct.pack('%df'%len(feature), *feature))
                dest_file.close()

                self.lock=False
                return True
            print("waiting")

    def compare(self, feature, rank_N=1):
        while True:
            if not self.lock:
                self.lock=True
                if len(self.list)==0 or len(self.list)<rank_N:
                    return ["db is too small"]
                score = np.dot(np.array(self.db), feature.transpose())
                self.lock=False
                score_index_sorted = np.argsort(score, axis=0) # db_length*feature_num
                expected_index = score_index_sorted[-rank_N:][:]
                result=[]
                for i in range(feature.shape[0]):
                    result.append([])
                    for j in range(rank_N):
                        result_index = expected_index[j][i]
                        result[-1].append([self.list[result_index], score[result_index, i]])
                return result
            print("waiting")


class FeatureDB_redis():
    def __init__(self, host, port, \
                 password, db_index, batch=10000, thread_num=None):
        self.redis = RedisClass(host, port, password, db_index)
        self.lock = False
        # self.gen_dict()
        self.batch = batch
        self.thread_num=thread_num

    def load_db(self):
        self.lock = True
        self.redis.get_all()
        self.lock = False
    
    def gen_dict(self):
        self.db_dict = {}
        for (k, v) in zip(self.redis.key_list, self.redis.feature_list):
            self.db_dict[k]=v

    def exists(self, id):
        while True:
            if not self.lock:
                # if id in self.db_dict.keys():
                if id in self.redis.key_list:
                    return True
                else:
                    return False
            # print("waiting")

    def find(self, id):
        try:
            index = self.redis.key_list.index(id)
            feature = self.redis.feature_list[index]
            return index,feature
        except:
            return -1,None
    
    def add(self, id, feature):
        assert feature.shape[0]==512
        if self.exists(id):
            # print("exists!")
            return False
        while True:
            if not self.lock:
                self.lock=True
                # self.db_dict[id]=feature
                self.redis.key_list=[id]+self.redis.key_list
                self.redis.feature_list=[feature]+self.redis.feature_list
                self.lock=False
                return True

    def delet(self, id):
        # if not self.exists(id):
        index, feature = self.find(id)
        if index==-1:
            return feature,False
        while True:
            if not self.lock:
                self.lock=True
                # self.db_dict.pop(id)
                del self.redis.feature_list[index]
                del self.redis.key_list[index]
                self.lock=False
                return feature, True

    def change(self, id, feature):
        # if not self.exists(id):
        assert feature.shape[0]==512
        index, old_feature = self.find(id)
        if index==-1:
            return old_feature, False
        while True:
            if not self.lock:
                self.lock=True
                # self.db_dict[id]=feature
                self.redis.key_list[index]=id
                self.redis.feature_list[index]=feature
                self.lock=False
                return old_feature, True

    def compare(self, features, rank_N=1):
        '''
        @features: [array]
        '''
        while True:
            if not self.lock:
                self.lock=True
                # keys_list = [k for k in self.db_dict.keys()]
                # feature_list = [v for v in self.db_dict.values()]
                keys_list = self.redis.key_list
                feature_list = self.redis.feature_list
                self.lock=False
                
                # print(len(keys_list), len(feature_list))
                
                result = []
                if len(keys_list) < rank_N:
                    print("there is no enough data for compare")
                    for i in range(len(features)):
                        result.append([])
                        for j in range(rank_N):
                            result[-1].append([0, 0])
                    return result
                if len(feature_list)<=self.batch:
                    score = np.dot(np.array(feature_list), np.array(features).transpose())
                    score = np.absolute(score)
                    score_index_sorted = np.argsort(score, axis=0) # db_length*feature_num
                    expected_index = score_index_sorted[-rank_N:][:]
                    for i in range(len(features)):
                        result.append([])
                        for j in range(rank_N):
                            result_index = expected_index[j][i]
                            result[-1].append([keys_list[result_index], score[result_index, i]])
                    return result
                else:
                    queue_length = (len(keys_list)//self.batch+1) if len(keys_list)%self.batch!=0 else len(keys_list)//self.batch
                    self.thread_num = self.thread_num if self.thread_num is not None else queue_length
                    queueLock = threading.Lock()
                    workQueue = queue.Queue( queue_length )
                    threads = []

                    # 创建新线程
                    for i in range(self.thread_num):
                        thread = myThread_compare(features, rank_N, workQueue, queueLock)
                        thread.start()
                        threads.append(thread)

                    # 填充队列
                    queueLock.acquire()
                    
                    for i in range(queue_length-1):
                        # print(index_pair)
                        workQueue.put([ i*self.batch, feature_list[i*self.batch: (i+1)*self.batch] ])
                    workQueue.put([ (queue_length-1)*self.batch, feature_list[(queue_length-1)*self.batch: ] ])
                    
                    queueLock.release()
                    
                    # 等待队列清空
                    while not workQueue.empty():
                        pass
                    # 通知线程是时候退出
                    for t in threads:
                        t.exit()

                    score_list = []
                    index_list = []
                    # 等待所有线程完成
                    for t in threads:
                        t.join()
                        for score,index in t.result_list:
                            score_list.append(score)
                            index_list.append(index)
                    # print("recv done",score_list )
                    score_list = np.stack(score_list, axis=1).squeeze(axis=0)# (que_len*rank_N)*f_num
                    index_list = np.stack(index_list, axis=1).squeeze(axis=0)
                    # print("combined score list", score_list.shape,score_list,index_list)
                    score = -np.sort(-score_list, axis=0)
                    score_index = -np.argsort(-score_list, axis=0)
                    score = np.sort(score_list, axis=0)
                    score_index = np.argsort(score_list, axis=0)
                    # print("sorted score list", score[:5,:], score_index[:5,:])
                    for i in range(len(features)):
                        result.append([])
                        for j in range(rank_N):
                            # print([keys_list[index_list[score_index[j, i], i]], score[j, i]])
                            # print(index_list[score_index[-(j+1), i], i])
                            result[-1].append([keys_list[index_list[score_index[-(j+1), i], i]], score[-(j+1), i]])
                    return result
                    
                    
                    

