import random
import numpy as np
import datetime

def tid_maker():
    return '{0:%Y-%m-%d-%H-%M-%S-%f}'.format(datetime.datetime.now())


feature_file_path = "test.txt"
test_num = 100000
feature_list = []
print("start", tid_maker())

# for i in range(test_num):
#     feature = [np.random.rand(512)]
#     feature_list.append(feature)

import sys
sys.path.append('/home/ubuntu/data3/lk/pycharm_projects/FaceSystem')
from configs.user_settings import redis_host, redis_port, redis_password, redis_db_index
import redis

import queue
import threading
class myThread_redis (threading.Thread):
    # def __init__(self, pipe, q, lock, exitFlag=0):
    def __init__(self, host, port, password, db_index, \
                q, lock, exitFlag=0):
        threading.Thread.__init__(self)
        # self.r = pipe
        # self.pipe = pipe
        self.pool = redis.ConnectionPool(host=host, port=port, password=password, db=db_index)
        self.r = redis.StrictRedis(connection_pool=self.pool)
        self.pipe = self.r.pipeline()
        
        self.q = q

        self.lock = lock
        self.exitFlag = exitFlag
        self.result_list = []

        # self.recorder = Recorder()
    
    def run(self):
        self.process_data()

    def process_data(self):
        queueLock = self.lock
        workQueue = self.q
        q = self.q
        pipe = self.pipe
        while not self.exitFlag:
            queueLock.acquire()
            if not workQueue.empty():
                keys = q.get()

                queueLock.release()
                for key in keys:
                    pipe.lrange(key, 0, -1)
                print("pip ready done",tid_maker())
                result = pipe.execute()
                print("thread read done",tid_maker())
                queueLock.acquire()
                self.result_list.append([keys, result])
                queueLock.release()
            else:
                queueLock.release()
                # time.sleep(1)
    
    def exit(self):
        self.exitFlag=1


class RedisClass():
    def __init__(self, host, port, \
                 password, db_index):
        self.host = host
        self.port = port
        self.password = password
        self.db_index = db_index
        self.pool = redis.ConnectionPool(host=self.host, port=self.port, password=self.password, db=self.db_index)
        self.r = redis.StrictRedis(connection_pool=self.pool)
        self.key_list = []
        self.feature_list = []
        
        self.batch=100000
        self.thread_num=None
        # self.get_all()

    def get_all(self):
        '''
        :return: [id, id...],[array, array]
        '''
        # print(self.r)
        pipe = self.r.pipeline()
        self.key_list = []
        self.key_list_temp = []
        keys = self.r.keys()
        # keys=[]
        r_end = self.r.scan_iter("*")
        for k in r_end:
            keys.append(k)
        keys=list(set(keys))
        # self.r.close() # TODO
        # keys = keys[:self.batch] # TODO
        # print(keys[:2])
        print("keys got",tid_maker())
        
        # print(keys)
        if len(keys)>self.batch:
            queue_length = (len(keys)//self.batch+1)
            self.thread_num = self.thread_num if self.thread_num is not None else queue_length
            queueLock = threading.Lock()
            workQueue = queue.Queue( queue_length )
            threads = []

            # 创建新线程
            for i in range(self.thread_num):
                # pipe = self.r.pipeline()
                # thread = myThread_redis(pipe, workQueue, queueLock)
                # thread = myThread_redis(self.r, workQueue, queueLock) # TODO
                thread = myThread_redis(self.host, self.port, self.password, self.db_index, workQueue, queueLock)
                thread.start()
                threads.append(thread)
            print("thread init done",tid_maker())

            # 填充队列
            queueLock.acquire()
            
            for i in range(queue_length-1):
                # print(index_pair)
                workQueue.put(keys[i*self.batch: (i+1)*self.batch])
            workQueue.put(keys[i*self.batch: ])
            print("queue init done",tid_maker())
            
            queueLock.release()
            
            # 等待队列清空
            while not workQueue.empty():
                pass

            # 通知线程是时候退出
            for t in threads:
                t.exit()

            print("thread exit done",tid_maker())
            keys_final = []
            result_final = []
            # 等待所有线程完成
            for t in threads:
                t.join()
                for keys_temp, results_temp in t.result_list:
                    keys_final.extend(keys_temp)
                    result_final.extend(results_temp)
            print("final keys length", len(keys_final)) # 结果整合的耗时
            self.feature_list = []
            for i in range(len(keys_final)):
                self.key_list.append(keys_final[i].decode())
                self.feature_list.append(np.array(result_final[i], dtype='float32'))
        else:
            for key in keys:
                pipe.lrange(key, 0, -1)
                self.key_list_temp.append(key)
            self.feature_list = []
            # pipe.mget(self.key_list_temp) # TODO
            # print("pip line prepare",tid_maker())
            # result = pipe.execute()
            print("straightly exe pipline",tid_maker())
            
            for (k, v) in zip(self.key_list_temp, pipe.execute()):
                if len(v) != 512:
                    print("the value for %s is invalid"%(k))
                    continue
                self.key_list.append(k.decode())
                self.feature_list.append(np.array(v, dtype='float32'))
        # print(self.key_list)
        # return self.key_list, self.feature_list

    def compare(self, features, rankn=1):
        '''
        :param features: [array, array]
        :param rankn:
        :return: [[[id, score]...]]
        '''
        self.get_all()
        result = []
        if len(features) == 0:
            return result
        # print(self.key_list)

        if len(self.key_list) < rankn:
            print("there is no enough data for compare")
            for i in range(len(features)):
                result.append([])
                for j in range(rankn):
                    result[-1].append([0, 0])
            return result
        if len(self.key_list)<=10000:
            score = np.dot(np.array(self.feature_list), np.array(features).transpose())
            score = np.absolute(score)
            score_index_sorted = np.argsort(score, axis=0) # db_length*feature_num
            expected_index = score_index_sorted[-rankn:][:]
            for i in range(len(features)):
                result.append([])
                for j in range(rankn):
                    result_index = expected_index[j][i]
                    result[-1].append([self.key_list[result_index], score[result_index, i]])
            return result

    def refresh(self):
        self.get_all()


sdly_db = RedisClass(redis_host, redis_port, redis_password, redis_db_index)
print("redis init done",tid_maker())
sdly_db.get_all()
print("read done",tid_maker())
feature_arr = sdly_db.feature_list
print("get %d record"%(len(feature_arr)))

unkown_feature= [np.random.rand(512)]
score = np.dot(feature_arr, np.array(unkown_feature).transpose())
print("score cal done",tid_maker())# 耗时最长，10w-5s左右
score = np.absolute(score)
score_index_sorted = np.argsort(score, axis=0)
print("score sort done",tid_maker()) # 耗时最少
