# config.py


cfg = {
    'name': 'Resnet50',
    'min_sizes': [[16, 32], [64, 128], [256, 512]],
    'steps': [8, 16, 32],
    'variance': [0.1, 0.2],
    'clip': False,
    'loc_weight': 2.0,
    'gpu_train': False,
    # 'batch_size': 24,
    'batch_size': 6,
    'ngpu': 4,
    'epoch': 100,
    'decay1': 70,
    'decay2': 90,
    'image_size': 840,
    'pretrain': False,
    'return_layers': {'layer2': 1, 'layer3': 2, 'layer4': 3},
    'in_channel': 256,
    'out_channel': 256
}

# cfg = {
#     'name': 'FaceBoxes',
#     #'min_dim': 1024,
#     #'feature_maps': [[32, 32], [16, 16], [8, 8]],
#     # 'aspect_ratios': [[1], [1], [1]],
#     'min_sizes': [[32, 64, 128], [256], [512]],
#     'steps': [32, 64, 128],
#     'variance': [0.1, 0.2],
#     'clip': False,
#     'loc_weight': 1,
#     'landm_weight': 0.5,
#     # 'landm_weight': 4.0,
#     'gpu_train': True
# }