import math


def NME(land_dets, land_gts):
    # print(land_dets, land_gts)
    eye_dist = math.pow((land_gts[5]-land_gts[6])**2+(land_gts[0]-land_gts[1])**2, 0.5)
    dist=0
    for i in range(5):
        dist += math.pow((land_dets[i]-land_gts[i])**2+(land_dets[i+5]-land_gts[i+5])**2, 0.5)
    dist /= 5*eye_dist
    return dist