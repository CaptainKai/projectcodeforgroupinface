import torch
from torch.utils.data import DataLoader

multi_gpus = False
from .config import cfg

from .layers.functions.prior_box import PriorBox
# from .utils.nms_wrapper import nms
from .utils.nms.py_cpu_nms import py_cpu_nms
import cv2
from .models.retinaface import RetinaFace
# from .models.faceboxes import FaceBoxes
from .utils.box_utils import decode,decode_landm
from .utils.model_handle import load_model
import time
import numpy as np

# import datetime
# curtime1 = datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S.%f')  
# print("send frame time = ", curtime1)

class Face_Detector():
    def __init__(self, top_k=5000, \
                 nms_threshold=0.4, keep_top_k=750, cpu=True):
        # self.confidence_threshold=confidence_threshold
        self.top_k = top_k
        self.nms_threshold = nms_threshold
        self.keep_top_k = keep_top_k
        # self.vis_thres = vis_thres
        self.device = torch.device("cpu" if cpu else "cuda")
        self.network = 'resnet50' # TODO
        # self.device = torch.device("cpu")
        # self.cpu = cpu

    def load_model(self, model_path):

        self.net = RetinaFace(cfg = cfg, phase = 'test')    # initialize detector
        self.net = load_model(self.net, model_path, True)
        self.net.eval()

    def detect(self, img, threshold=0.4, resize=1):
        # torch.cuda.set_device(0)
        # os.environ["CUDA_VISIBLE_DEVICES"]="0"
        # self.net = self.net.to(self.device)
        img = np.float32(img)
        
        target_size = 1600
        max_size = 2150
        im_shape = img.shape
        im_size_min = np.min(im_shape[0:2])
        im_size_max = np.max(im_shape[0:2])
        resize = float(target_size) / float(im_size_min)
        # prevent bigger axis from being more than max_size:
        if np.round(resize * im_size_max) > max_size:
            resize = float(max_size) / float(im_size_max)
        if resize != 1:
            img = cv2.resize(img, None, None, fx=resize, fy=resize, interpolation=cv2.INTER_LINEAR)
        # print(img.shape)
        im_height, im_width, _ = img.shape
        
        scale = torch.Tensor([img.shape[1], img.shape[0], img.shape[1], img.shape[0]])
        # scale_landm = torch.Tensor([img.shape[1], img.shape[0], img.shape[1], img.shape[0], img.shape[1],
                                    # img.shape[0], img.shape[1], img.shape[0], img.shape[1], img.shape[0]])
        img -= (104, 117, 123)
        img = img.transpose(2, 0, 1)
        img = torch.from_numpy(img).unsqueeze(0)

        img = img.to(self.device)
        scale = scale.to(self.device)
        # scale_landm = scale_landm.to(self.device)

        # loc, conf = self.net(img)  # forward pass # TODO
        loc, conf, landms = self.net(img)  # forward pass # TODO

        priorbox = PriorBox(cfg, image_size=(im_height, im_width))
        priors = priorbox.forward()
        priors = priors.to(self.device)
        prior_data = priors.data
        boxes = decode(loc.data.squeeze(0), prior_data, cfg['variance'])
        boxes = boxes * scale / resize
        boxes = boxes.cpu().numpy()

        scores = conf.squeeze(0).data.cpu().numpy().max(axis=1) # TODO

        label_index = np.argmax(conf.squeeze(0).data.cpu().numpy(), axis=1) # TODO
        landms = decode_landm(landms.data.squeeze(0), prior_data, cfg['variance'])
        scale1 = torch.Tensor([img.shape[3], img.shape[2], img.shape[3], img.shape[2],
                               img.shape[3], img.shape[2], img.shape[3], img.shape[2],
                               img.shape[3], img.shape[2]])
        scale1 = scale1.to(self.device)
        landms = landms * scale1 / resize
        landms = landms.cpu().numpy()

        no_background_mask = label_index>0
        scores = scores[no_background_mask]
        boxes = boxes[no_background_mask]
        landms = landms[no_background_mask]
        label_index = label_index[no_background_mask]

        # ignore low scores
        inds = np.where(scores > threshold)[0]
        boxes = boxes[inds]
        landms = landms[inds]
        scores = scores[inds]
        label_index = label_index[inds]
        # print(label_index[:5])


        # keep top-K before NMS
        order = scores.argsort()[::-1][:self.top_k]
        boxes = boxes[order]
        landms = landms[order]
        scores = scores[order]
        label_index = label_index[order]

        # do NMS
        # dets = np.hstack((boxes, scores[:, np.newaxis])).astype(np.float32, copy=False)
        dets = np.hstack((boxes, scores[:, np.newaxis], landms, label_index[:, np.newaxis])).astype(np.float32, copy=False)
        #keep = py_cpu_nms(dets, self.nms_threshold)
        keep = py_cpu_nms(dets[:,:5], self.nms_threshold, force_cpu=(not multi_gpus))
        dets = dets[keep, :]

        # keep top-K faster NMS
        dets = dets[:self.keep_top_k, :]

        return dets

    def detect_imgs(self, frame_list, threshold=0.4):
        dets_list = []

        for frame in frame_list:
            # resize = max( [ float(max_length)/max(frame.shape), float(min_length)/min(frame.shape[:2]) ] )
            dets = self.detect(frame, threshold=threshold)
            dets_list.append(dets)
        return dets_list
    
    def detect_video(self, frame_list, threshold=0.4):

        frame_list_tensor = []
        for idx, frame in enumerate(frame_list):
            frame = np.float32(frame)
            target_size = 1600
            max_size = 2150
            im_shape = frame.shape
            im_size_min = np.min(im_shape[0:2])
            im_size_max = np.max(im_shape[0:2])
            resize = float(target_size) / float(im_size_min)
        # prevent bigger axis from being more than max_size:
            if np.round(resize * im_size_max) > max_size:
               resize = float(max_size) / float(im_size_max)
            
            frame = cv2.resize(frame, None, None, fx=resize, fy=resize, interpolation=cv2.INTER_LINEAR)
            if idx==0:
                im_height, im_width, _ = frame.shape
                scale = torch.Tensor([frame.shape[1], frame.shape[0], frame.shape[1], frame.shape[0]])
                scale_landm = torch.Tensor([frame.shape[1], frame.shape[0], frame.shape[1], frame.shape[0], frame.shape[1],
                                            frame.shape[0], frame.shape[1], frame.shape[0], frame.shape[1], frame.shape[0]])
            frame -= (104, 117, 123)
            frame = frame.transpose(2, 0, 1)
            frame = torch.from_numpy(frame).unsqueeze(0)
            frame_list_tensor.extend(frame)

        dl = DataLoader(frame_list_tensor, batch_size=len(frame_list_tensor), shuffle=False)
        # loc, conf = self.net(img)  # forward pass # TODO
        for img_list in dl:
            img_list = img_list.to(self.device)
            loc_list, conf_list, landm_list = self.net(img_list)  # forward pass # TODO

        priorbox = PriorBox(cfg, image_size=(im_height, im_width))
        priors = priorbox.forward()
        priors = priors.to(self.device)
        prior_data = priors.data

        dets_list = []
        # t0 = time.time()
        for loc, conf, landms in zip(loc_list, conf_list, landm_list):
            boxes = decode(loc.data.squeeze(0), prior_data, cfg['variance'])
            boxes = boxes * scale / resize
            boxes = boxes.cpu().numpy()

            scores = conf.squeeze(0).data.cpu().numpy().max(axis=1) # TODO

            label_index = np.argmax(conf.squeeze(0).data.cpu().numpy(), axis=1) # TODO
            landms = decode_landm(landms.data.squeeze(0), prior_data, cfg['variance'])
            scale1 = torch.Tensor([frame.shape[3], frame.shape[2], frame.shape[3], frame.shape[2],
                               frame.shape[3], frame.shape[2], frame.shape[3], frame.shape[2],
                               frame.shape[3], frame.shape[2]])
            scale1 = scale1.to(self.device)
            landms = landms * scale1 / resize
            landms = landms.cpu().numpy()

            no_background_mask = label_index>0
            scores = scores[no_background_mask]
            boxes = boxes[no_background_mask]
            landms = landms[no_background_mask]
            label_index = label_index[no_background_mask]

            # ignore low scores
            inds = np.where(scores > threshold)[0]
            boxes = boxes[inds]
            landms = landms[inds]
            scores = scores[inds]
            label_index = label_index[inds]
            # print(label_index[:5])


            # keep top-K before NMS
            order = scores.argsort()[::-1][:self.top_k]
            boxes = boxes[order]
            landms = landms[order]
            scores = scores[order]
            label_index = label_index[order]

            # do NMS
            # dets = np.hstack((boxes, scores[:, np.newaxis])).astype(np.float32, copy=False)
            dets = np.hstack((boxes, scores[:, np.newaxis], landms, label_index[:, np.newaxis])).astype(np.float32, copy=False)
            #keep = py_cpu_nms(dets, self.nms_threshold)
            keep = py_cpu_nms(dets[:,:5], self.nms_threshold, force_cpu=True)
            dets = dets[keep, :]

            # keep top-K faster NMS
            dets = dets[:self.keep_top_k, :]
            if dets is None:
                dets=[]
            dets_list.append(dets)
        # t1 = time.time()
        # print("after-process: ", t1-t0)

        return dets_list
    
    def run(self, img, threshold=0.4, max_length=1024, min_length=1024):
        resize = max( [ float(max_length)/max(img.shape), float(min_length)/min(img.shape[:2]) ] )
        dets = self.detect(img, threshold=threshold, resize=resize)
        return dets