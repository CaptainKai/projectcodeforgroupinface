import torch
import torch.nn as nn
import torch.nn.functional as F
from torch.nn import Parameter


class ConvPrelu(nn.Module):

    def __init__(self, in_channels, out_channels, filter='gaussian', prelu_init=0.25, mean=0, std=0.01,**kwargs):
        super(ConvPrelu, self).__init__()
        if filter=="xavier":
            self.conv = nn.Conv2d(in_channels, out_channels, bias=False, **kwargs)
            torch.nn.init.xavier_uniform_(self.conv.weight)
            self.prelu = nn.PReLU(num_parameters=out_channels, init=prelu_init) # default=false, init=0.25
        elif filter=="gaussian":
            self.conv = nn.Conv2d(in_channels, out_channels, bias=False, **kwargs)
            torch.nn.init.normal_(self.conv.weight, mean=mean, std=std)# default=（0,0.01）
            self.prelu = nn.PReLU(num_parameters=out_channels, init=prelu_init)

    def forward(self, x):
        x = self.conv(x)
        x = self.prelu(x)
        return x

class SeModule(nn.Module):

    def __init__(self, in_channels):
        super(SeModule, self).__init__()
        
        
        self.globalAvgPool = nn.AdaptiveAvgPool2d(1)
        self.fc1 = nn.Linear(in_features=in_channels, out_features=round(in_channels / 16))
        self.relu = nn.ReLU(inplace=True)
        self.fc2 = nn.Linear(in_features=round(in_channels / 16), out_features=in_channels)
        self.sigmoid = nn.Sigmoid()
    
    def forward(self, x):
        original_out = x
        
        out = self.globalAvgPool(x)
        out = out.view(original_out.size(0), original_out.size(1))
        out = self.fc1(out)
        out = self.relu(out)
        out = self.fc2(out)
        out = self.sigmoid(out)
        out = out.view(original_out.size(0), original_out.size(1),1,1)
        out = out * original_out

        return out

class SimpleResidualUnit(nn.Module):
    
    def __init__(self, in_channels, kernel_size=3, stride=1, padding=1, use_se=False):
        super(SimpleResidualUnit, self).__init__()
        self.conv1 = ConvPrelu(in_channels, in_channels, kernel_size=kernel_size, stride=stride, padding=padding)
        self.conv2 = ConvPrelu(in_channels, in_channels, kernel_size=kernel_size, stride=stride, padding=padding)
        self.use_se = use_se
        if self.use_se:
            self.se = SeModule(in_channels)
    
    def forward(self, x):
        residual = x

        out = self.conv1(x)
        out = self.conv2(out)

        if self.use_se:
            out = self.se(out)

        out+=residual

        return out


class SimpleResidualBackbone(nn.Module):

    def __init__(self, layers, use_se=False, block=SimpleResidualUnit, phase='train'):
        super(SimpleResidualBackbone, self).__init__()
        self.phase = phase
        self.use_se = use_se
        # self.num_classes = num_classes

        self.conv1 = ConvPrelu(3, 64, kernel_size=3, stride=2, padding=1, filter='xavier')
        self.layer1 = self._make_layer(64, base_layer=block,layers_num=layers[0])

        self.conv2 = ConvPrelu(64, 128, kernel_size=3, stride=2, padding=1, filter='xavier')
        self.layer2 = self._make_layer(128, base_layer=block,layers_num=layers[1])

        self.conv3 = ConvPrelu(128, 256, kernel_size=3, stride=2, padding=1, filter='xavier')
        self.layer3 = self._make_layer(256, base_layer=block,layers_num=layers[2])

        self.conv4 = ConvPrelu(256, 512, kernel_size=3, stride=2, padding=1, filter='xavier')
        self.layer4 = self._make_layer(512, base_layer=block,layers_num=layers[3])

        self.fc5 = nn.Linear(512*7*7, 512)

        if self.phase == 'train':
            for m in self.modules():
                if isinstance(m, nn.Conv2d):
                    if m.bias is not None:
                        nn.init.constant_(m.bias, 0.0)
                if isinstance(m, nn.Linear):
                    # m.bias.data.fill_(0)
                    nn.init.constant_(m.bias, 0.0)
                    torch.nn.init.xavier_uniform_(m.weight.data)

    def _make_layer(self, in_channels, base_layer, layers_num):
        layers=[]
        for i in range(layers_num):
            layers.append(base_layer(in_channels, use_se=self.use_se))
            self.use_se = False# TODO
        return nn.Sequential(*layers)


    def forward(self, x):
        conv1 = self.conv1(x)
        layer1 = self.layer1(conv1)
        conv2 = self.conv2(layer1)
        layer2 = self.layer2(conv2)
        conv3 = self.conv3(layer2)
        layer3 = self.layer3(conv3)
        conv4 = self.conv4(layer3)
        layer4 = self.layer4(conv4)
        layer4 = layer4.view(layer4.size(0), -1)

        feature = self.fc5(layer4)
        # print("backbone device: %d is working"%(torch.cuda.current_device()))
        return feature


    def save(self, file_path):
        with open(file_path, 'wb') as f:
            torch.save(self.state_dict(), f)
    
    def get_layer4(self, x):
        conv1 = self.conv1(x)
        layer1 = self.layer1(conv1)
        conv2 = self.conv2(layer1)
        layer2 = self.layer2(conv2)
        conv3 = self.conv3(layer2)
        layer3 = self.layer3(conv3)
        conv4 = self.conv4(layer3)
        layer4 = self.layer4(conv4)
        layer4 = layer4.view(layer4.size(0), -1)

        return layer4


class SimpleResidualBackbone_smask(nn.Module):

    def __init__(self, layers, use_se=False, block=SimpleResidualUnit, phase='test'):
        super(SimpleResidualBackbone_smask, self).__init__()
        self.phase = phase
        self.use_se = use_se
        # self.num_classes = num_classes

        self.conv1 = ConvPrelu(3, 64, kernel_size=3, stride=2, padding=1, filter='xavier')
        self.layer1 = self._make_layer(64, base_layer=block,layers_num=layers[0])

        self.conv2 = ConvPrelu(64, 128, kernel_size=3, stride=2, padding=1, filter='xavier')
        self.layer2 = self._make_layer(128, base_layer=block,layers_num=layers[1])

        self.conv3 = ConvPrelu(128, 256, kernel_size=3, stride=2, padding=1, filter='xavier')
        self.layer3 = self._make_layer(256, base_layer=block,layers_num=layers[2])

        self.conv4 = ConvPrelu(256, 512, kernel_size=3, stride=2, padding=1, filter='xavier')
        self.layer4 = self._make_layer(512, base_layer=block,layers_num=layers[3])

        self.fc5 = nn.Linear(512*7*7, 512)

        if self.phase == 'train':
            for m in self.modules():
                if isinstance(m, nn.Conv2d):
                    if m.bias is not None:
                        nn.init.constant_(m.bias, 0.0)
                if isinstance(m, nn.Linear):
                    # m.bias.data.fill_(0)
                    nn.init.constant_(m.bias, 0.0)
                    torch.nn.init.xavier_uniform_(m.weight.data)

    def _make_layer(self, in_channels, base_layer, layers_num):
        layers=[]
        for i in range(layers_num):
            layers.append(base_layer(in_channels, use_se=self.use_se))
        return nn.Sequential(*layers)


    def forward(self, x, ldm):
        conv1 = self.conv1(x)

        mask = torch.ones(conv1_1.shape).to('cuda')*0.5 # batch c w h
        # print(ldm.shape)
        # ldm # batch 1 10
        # dist_em = ldm[:, 0, 5]-ldm[:, 0, 1]
        xmax = conv1_1.shape[2]
        ymax = ( ldm[:, 1]+ldm[:, 5] )/2*xmax/112
        # print(mask[0, 0, 0:xmax, 0:ymax])
        # mask[:, :, 0:xmax, 0:ymax] = 1
        for b in range(conv1_1.shape[0]):
            mask[b, :, 0:ymax[b], 0:xmax] = 1
        
        layer1 = self.layer1(conv1)
        conv2 = self.conv2(layer1)
        layer2 = self.layer2(conv2)
        conv3 = self.conv3(layer2)
        layer3 = self.layer3(conv3)
        conv4 = self.conv4(layer3)
        layer4 = self.layer4(conv4)
        layer4 = layer4.view(layer4.size(0), -1)

        feature = self.fc5(layer4)
        # print("backbone device: %d is working"%(torch.cuda.current_device()))
        return feature


    def save(self, file_path):
        with open(file_path, 'wb') as f:
            torch.save(self.state_dict(), f)


def SimpleResnet_20(**kwargs):
    """Constructs a SimpleResnet_20 model.
    """
    model = SimpleResidualBackbone([1, 2, 4, 1], use_se=False, block=SimpleResidualUnit, **kwargs)
    return model


def SimpleResnet_36(**kwargs):
    """Constructs a SimpleResnet_36 model.
    """
    model = SimpleResidualBackbone([2, 4, 8, 2], use_se=False, block=SimpleResidualUnit, **kwargs)
    return model


def SimpleResnet_64(**kwargs):
    """Constructs a SimpleResnet_64 model.
    """
    model = SimpleResidualBackbone([3, 8, 16, 3], use_se=False, block=SimpleResidualUnit, **kwargs)
    return model


def SimpleResnet_20_Se(**kwargs):
    """Constructs a SimpleResnet_20 model.
    """
    model = SimpleResidualBackbone([1, 2, 4, 1], use_se=True, block=SimpleResidualUnit, **kwargs)
    return model


def SimpleResnet_36_Se(**kwargs):
    """Constructs a SimpleResnet_36 model.
    """
    model = SimpleResidualBackbone([2, 4, 8, 2], use_se=True, block=SimpleResidualUnit,  **kwargs)
    return model


def SimpleResnet_64_Se(**kwargs):
    """Constructs a SimpleResnet_64 model.
    """
    model = SimpleResidualBackbone([3, 8, 16, 3], use_se=True, block=SimpleResidualUnit, **kwargs)
    return model