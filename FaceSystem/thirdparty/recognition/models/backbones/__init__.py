from .simple_resnet import SimpleResnet_20, SimpleResnet_36, SimpleResnet_64
from .simple_resnet import SimpleResnet_20_Se, SimpleResnet_36_Se, SimpleResnet_64_Se
from .classifier import *
from .resnest import *
from .senet import *
from .mapping_net import *
from .resnet import *
# from .hpda import TestNet
# from .resnet import ResNet_50 as resnet50
# from .resnet import ResNet_101 as resnet101
# from .resnet import ResNet_152 as resnet152
