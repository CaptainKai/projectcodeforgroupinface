from .models import *
import torchvision.transforms as transforms
import torch
from torch.utils.data import DataLoader
from torchvision.transforms import functional as F

import cv2


recogniton_transform = transforms.Compose([
    transforms.ToTensor(),  # range [0, 255] -> [0.0,1.0]
    transforms.Normalize(mean=(0.5, 0.5, 0.5), std=(0.5, 0.5, 0.5))  # range [0.0, 1.0] -> [-1.0,1.0]
])


class FeatureExtractor():

    def __init__(self, backbone_name, head_name=None, num_class=10):
        self.backbone = None
        self.head = None
        self.backbone_name = backbone_name
        self.head_name = head_name
        self.num_class = num_class

    def load_model(self, backbone_path=None, head_path=None, input_size=[112,112]):
        try:
            self.backbone = models.get_model(self.backbone_name)
        except:
            self.backbone = models.get_model(self.backbone_name, input_size=input_size)
        if self.head_name is not None:
            self.head = models.get_model(self.backbone_name, in_features=512, out_features=self.num_class)
        if backbone_path is None and head_path is None:
            print("at least one argument!")
            exit(0)
        if backbone_path is not None and self.backbone is not None:
            self.backbone.load_state_dict(torch.load(backbone_path, map_location=lambda storage, loc: storage))
            self.backbone.eval()
        if head_path is not None and self.head is not None:
            self.head.load_state_dict(torch.load(head_path))
            self.head.eval()

    def extractDeepFeature(self, img, flop=False, transform=recogniton_transform):
        '''

        :param img:
        :param flop:
        :param transform:
        :return: array
        '''
        if img.size[0] != 112 or img.size[1] != 112:
            print("resizing")
            img = img.resize((112, 112))
        img = transform(img)
        if flop:
            img_ = transform(F.hflip(img))
            img_ = img_.unsqueeze(0)
            img = img.unsqueeze(0)
            feature = torch.cat((self.backbone(img), self.backbone(img_)), 1)[0]
        else:
            img = img.unsqueeze(0)
            feature = self.backbone(img)[0].detach().numpy()

        feature = feature/cv2.norm(feature)
        return feature

    def extractDeepFeatures(self, imgs, transform=recogniton_transform):
        '''

        :param imgs:
        :param transform:
        :return: [array, array]
        '''
        if len(imgs) == 0:
            return []
        img_list=[]
        for img in imgs:
            if img.size[0] != 112 or img.size[1] != 112:
                print("resizing")
                img = img.resize((112, 112))
            img = transform(img)
            img_list.extend(img.unsqueeze(0))
        # print(len(imgs))
        dl = DataLoader(img_list, batch_size=len(imgs), shuffle=False)
        result=[]
        for images in dl:
            features = self.backbone(images)
            for feature in features:
                feature = feature.detach().numpy()
                feature = feature/cv2.norm(feature)
                result.append(feature)
        return result
    
    def run(self, img, flop=False, transform=recogniton_transform):
        result = self.extractDeepFeature(img, flop, transform)
        return result
