from detection2 import *
import cv2
import os
def main():
    face_detector = Face_Detector()
    face_detector.load_model("/home/ubuntu/data3/lk/pycharm_projects/FaceSystem/thirdparty/detection2/weights/retinaface/Resnet50_Final.pth")
    # face_detector.load_model(BASE_DIR + "/detection/weights/mask_landmark_augue/Final_FaceBoxes.pth")
    detector_object = face_detector
    test_file_path = "/home/ubuntu/data3/lk/pycharm_projects/FaceSystem/thirdparty/detection2/7777.jpg"
    img_raw = cv2.imread(test_file_path, cv2.IMREAD_COLOR)
    dets_list = face_detector.detect_imgs([img_raw], threshold=0.6)
    # print(dets_list)
    
    for b in dets_list[0]:
                if b[4] < 0.6:
                    continue
                text = "{:.4f}".format(b[4])
                b = list(map(int, b))
                cv2.rectangle(img_raw, (b[0], b[1]), (b[2], b[3]), (0, 0, 255), 2)
                cx = b[0]
                cy = b[1] + 12
                cv2.putText(img_raw, text, (cx, cy),
                            cv2.FONT_HERSHEY_DUPLEX, 0.5, (255, 255, 255))

                # landms
                cv2.circle(img_raw, (b[5], b[6]), 1, (0, 0, 255), 4)
                cv2.circle(img_raw, (b[7], b[8]), 1, (0, 255, 255), 4)
                cv2.circle(img_raw, (b[9], b[10]), 1, (255, 0, 255), 4)
                cv2.circle(img_raw, (b[11], b[12]), 1, (0, 255, 0), 4)
                cv2.circle(img_raw, (b[13], b[14]), 1, (255, 0, 0), 4)
            # save image
    
    name = "/home/ubuntu/data3/lk/pycharm_projects/FaceSystem/thirdparty/detection2/testresult/" + "7777.jpg"
    cv2.imwrite(name, img_raw)



if __name__ == '__main__':
    main()