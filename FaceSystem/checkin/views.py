from thirdparty.utils import APIView


from thirdparty.utils import STD, UNIT_SIZE
from thirdparty.utils import findNonreflectiveSimilarity

from configs.settings import face_extractor, face_detector, logger
from configs.settings import sdly_db
from configs.user_settings import thread_num
from PIL import Image
import cv2
import numpy as np

import traceback
from thirdparty.utils import myThread
import threading
import queue

class ExtractFeatureAPI(APIView):
    # 人脸特征提取API
    def post(self, request):

        req_files = request.FILES.getlist("files")
        if len(req_files)==0:
            print("no file got, please check the request")
            result = [{"feature":[], "picked_id":"None", "exists": 0}]
            return self.success(result)
        threshold = float(request.data["threshold"])
        logger.info("threshold set as %f"%( threshold ))
        img_list = []
        # file_name_list = []
        for req_file in req_files:
            pil_img = Image.open(req_file).convert("RGB")

            img_list.append(pil_img)
        try:
            if len(img_list)>1000:
                
                result_list = []
                queueLock = threading.Lock()
                workQueue = queue.Queue(len(img_list))
                threads = []

                for i in range(thread_num):
                    thread = myThread(face_extractor, workQueue, queueLock)
                    thread.start()
                    threads.append(thread)
                queueLock.acquire()

                for id,img in enumerate(img_list):
                    # print(index_pair)
                    workQueue.put([id, img, None])
                queueLock.release()

                while not workQueue.empty():
                    pass
                for t in threads:
                    t.exit()
                for t in threads:
                    t.join()
                    result_list.extend(t.result_list)
                    result_list.sort(key=lambda x: x[0])
                    features = [x[1] for x in result_list]

            else:
                features = face_extractor.extractDeepFeatures(img_list)
        except:
            traceback.print_exc()
            logger.info("extract process broken!")
            return self.success("Internet Error")
        result = []
        try:
            com_result = sdly_db.compare(features)
        except:
            traceback.print_exc()
            logger.info("db process brocken!")
            return self.success("Redis Manipulation Error")
        try:
            for i in range(len(features)):
                result_dict = {}
                result_dict["feature"] = features[i]
                if com_result[i][0][1] >= threshold:
                    result_dict["picked_id"] = str(com_result[i][0][0])
                    result_dict["exists"] = 1
                else:
                    result_dict["picked_id"] = "None"
                    result_dict["exists"] = 0
                result.append(result_dict)
        except:
            traceback.print_exc()
            logger.info("compare process broken")
            return self.success("compare process broken")
        logger.info(str([(com_result[i][0][0], com_result[i][0][1]) for i in range(len(features))]))
        return self.success(result)


from thirdparty.utils import img2base64


class DetectAPI(APIView):
    def post(self, request):
        req_files = request.FILES.getlist("files")
        threshold = float(request.data["threshold"])
        max_length = float(request.data["max_length"])
        min_length = float(request.data["min_length"])

        logger.info("threshold %f"%( threshold ))
        logger.info("max_length %f" % max_length)
        logger.info("min_length %f" % min_length)
        # img_list = []
        frame_list = []
        for req_file in req_files:
            pil_img = Image.open(req_file).convert("RGB")
            cv_img = cv2.cvtColor(np.asarray(pil_img), cv2.COLOR_RGB2BGR)
            frame_list.append(cv_img)
        logger.info("data recieve process done: %d pics"%(len(frame_list)))
            # img_list.append(pil_img)
        # dets_list = face_detector.detect_video(frame_list, threshold=threshold)
        if len(frame_list)>2:
                
            result_list = []
            queueLock = threading.Lock()
            workQueue = queue.Queue(len(frame_list))
            threads = []

            for i in range(thread_num):
                thread = myThread(face_detector, workQueue, queueLock)
                thread.start()
                threads.append(thread)
            queueLock.acquire()

            for id,img in enumerate(frame_list):
                # print(index_pair)
                params = {'threshold':threshold, 'max_length':max_length, 'min_length':min_length}
                workQueue.put([id, img, params])
            queueLock.release()

            while not workQueue.empty():
                pass
            for t in threads:
                t.exit()
            for t in threads:
                t.join()
                result_list.extend(t.result_list)
                result_list.sort(key=lambda x: x[0])
                dets_list = [x[1] for x in result_list]
        else:
            dets_list = face_detector.detect_imgs(frame_list, threshold=threshold, max_length=max_length, min_length=min_length)
        logger.info("detect process done")
        final_com_results = []
        for dets, frame in zip(dets_list, frame_list):
            final_com_results.append([frame])
            for det in dets:
                score = det[4]
                landmark = det[5:15]
                src = np.zeros(10)  # [x1,y1,...]=>[x1,x2...y1,y2...]
                for i in range(5):
                    src[i] = landmark[i*2]
                for i in range(5):
                    src[i+5] = landmark[i*2+1]
                M = findNonreflectiveSimilarity(src, STD)
                cv_res = cv2.warpAffine(frame, M, (int(UNIT_SIZE), int(UNIT_SIZE)))
                final_com_results[-1].append([cv_res, score])
        result=[]
        for frame_result in final_com_results:
            if len(frame_result)==1:
                frame_info={"exists":False, "face_num":0, "info":[]}
            else:
                frame_info={"exists":True, "face_num":len(frame_result)-1, "info":[]}
                for det_result in frame_result[1:]:
                    det_info = {"det_frame":img2base64(det_result[0]), "score":det_result[1]}
                    frame_info["info"].append(det_info)
            result.append(frame_info)

        return self.success(result)


from configs.settings import temp_path
from thirdparty.utils import cluster, tid_maker
import os
class DetectVideoAPI(APIView):
    def post(self, request):

        req_file = request.FILES.get("file")
        detect_threshold = float(request.data["detect_threshold"])
        internal = int(request.data["internal"])
        max_length = float(request.data["max_length"])
        min_length = float(request.data["min_length"])

        logger.info("detect_threshold %f" % detect_threshold)
        logger.info("max_length %f" % max_length)
        logger.info("min_length %f" % min_length)
        logger.info("internal %d" % internal)
        time_id = tid_maker()
        destination_path = os.path.join(temp_path, time_id+".mp4")
        destination = open(destination_path, 'wb+')
        for chunk in req_file.chunks():
            destination.write(chunk)
        destination.close()

        logger.info("video recieved")

        video_reader = cv2.VideoCapture(destination_path)
        index = 0
        frame_list = []
        while True:
            success, frame = video_reader.read()
            if not success:
                logger.info("video read done")
                break
            if index%internal==0:
                frame_list.append(frame)
            index += 1
        logger.info("divided into %d pics" % len(frame_list))
        result = {"get_pic_num": len(frame_list)}

        os.remove(destination_path)

        final_com_results = []
        
        if len(frame_list)>2:
                
            result_list = []
            queueLock = threading.Lock()
            workQueue = queue.Queue(len(frame_list))
            threads = []

            for i in range(thread_num):
                thread = myThread(face_detector, workQueue, queueLock)
                thread.start()
                threads.append(thread)
            queueLock.acquire()

            for id,img in enumerate(frame_list):
                # print(index_pair)
                params = {'threshold':detect_threshold, 'max_length':max_length, 'min_length':min_length}
                workQueue.put([id, img, params])
            queueLock.release()

            while not workQueue.empty():
                pass
            for t in threads:
                t.exit()
            for t in threads:
                t.join()
                result_list.extend(t.result_list)
                result_list.sort(key=lambda x: x[0])
                dets_list = [x[1] for x in result_list]
        else:
        # dets_list = face_detector.detect_video(frame_list, threshold=detect_threshold, max_length=max_length, min_length=min_length)
            dets_list = face_detector.detect_imgs(frame_list, threshold=detect_threshold, max_length=max_length, min_length=min_length)
        logger.info("detect process done")
        extractor_list = []
        for dets, frame in zip(dets_list, frame_list):
            final_com_results.append([frame])
            final_com_results[-1].append([])
            for det in dets:
                landmark = det[5:15]
                src = np.zeros(10)  # [x1,y1,...]=>[x1,x2...y1,y2...]
                for i in range(5):
                    src[i] = landmark[i*2]
                for i in range(5):
                    src[i+5] = landmark[i*2+1]
                M = findNonreflectiveSimilarity(src, STD)
                cv_res = cv2.warpAffine(frame, M, (int(UNIT_SIZE), int(UNIT_SIZE)))
                pil_res = Image.fromarray(cv2.cvtColor(cv_res, cv2.COLOR_BGR2RGB))
                final_com_results[-1][1].append(cv_res)
                extractor_list.append(pil_res)        
        if len(extractor_list)==0:
            result["result_pic_num"] = 0
            result["result_info"] = []
            return self.success(result)
        
        try:
            if len(extractor_list)>1000:
                
                result_list = []
                queueLock = threading.Lock()
                workQueue = queue.Queue(len(extractor_list))
                threads = []

                for i in range(thread_num):
                    thread = myThread(face_extractor, workQueue, queueLock)
                    thread.start()
                    threads.append(thread)
                queueLock.acquire()

                for id,img in enumerate(extractor_list):
                    # print(index_pair)
                    workQueue.put([id, img, None])
                queueLock.release()

                while not workQueue.empty():
                    pass
                for t in threads:
                    t.exit()
                for t in threads:
                    t.join()
                    result_list.extend(t.result_list)
                    result_list.sort(key=lambda x: x[0])
                    features = [x[1] for x in result_list]
            else:
                features = face_extractor.extractDeepFeatures(extractor_list)
        except:
            traceback.print_exc()
            logger.info("extract error!")
            return self.success("Internet Error")
        # print(feature)
        logger.info("extraction process done")
        try:
            compare_result = cluster(features, threshold=0.6)
            feature_index = 0
            for frame_index, dets in enumerate(dets_list):
                for det_index, det in enumerate(dets):
                    compare_result[feature_index]=[compare_result[feature_index], det[4]]
                    compare_result[feature_index].append(frame_index)
                    compare_result[feature_index].append(det_index)
                    compare_result[feature_index].append(feature_index)
                    feature_index+=1

            result_sorted_score = sorted(compare_result, key=lambda x:x[1], reverse=True)
            result_sorted_key = sorted(result_sorted_score, key=lambda x:x[0])

            old_result = result_sorted_key[0]
            for singleresult in result_sorted_key[1:]:
                old_key = old_result[0]
                new_key = singleresult[0]
                if new_key!=old_key:
                    frame_index = old_result[2]
                    det_index = old_result[3]
                    features_index = old_result[4]
                    info = dets_list[frame_index][det_index]
                    det_frame = final_com_results[frame_index][1][det_index]
                    feature = features[features_index]
                    score = old_result[1]
                    id = old_result[0]
                    # if score>=threshold:
                    final_com_results[frame_index].append([info, feature, id, score, det_frame])
                    old_result = singleresult
                else:
                    continue

            frame_index = old_result[2]
            det_index = old_result[3]
            features_index = old_result[4]
            info = dets_list[frame_index][det_index]
            det_frame = final_com_results[frame_index][1][det_index]
            feature = features[features_index]
            score = old_result[1]
            id = old_result[0]
            final_com_results[frame_index].append([info, feature, id, score, det_frame])

            logger.info("recognition process done")

            final_com_results = [x for x in final_com_results if len(x)>2]
            result["result_pic_num"] = len(final_com_results)
            result["result_info"] = []

            result_info = []
            for idx, final_result in enumerate(final_com_results):
                result_info.append({"people_num" : len(final_result)-2,
                                    # "frame" : img2base64(final_result[0]), # encode
                                    "frame_info": []})
                frame_info = []
                for frame_result in final_result[2:]:
                    frame_info.append({
                                        "face_score": frame_result[0][4],
                                       "det_frame": img2base64(frame_result[-1]), # encode
                                       })
                result_info[-1]["frame_info"] = frame_info
            result["result_info"]=result_info
        except:
            traceback.print_exc()
            logger.info("compare Error")
            return self.success("compare Error")
        return self.success(result)


class DB_ADD_API_m(APIView):
    def post(self, request):
        ids = request.POST.getlist('ids',[])
        recv = request.POST.getlist('features',[])
        
        print(recv[0],ids[0])
        if len(recv)%512!=0:
            print("feature length invalid")
            return self.success("feature length invalid")
        if len(recv)//512!=len(ids):
            print("feature number does not match with id number")
            return self.success("feature number does not match with id number")
        for i in range(len(recv)//512):
            features.append([recv[i*512:(i+1)*512]])
        result = []
        for i in range(len(ids)):
            result.append({"id":ids[i], "flag":sdly_db.add(ids[i], np.array(features[i]))})
        
        return self.success(result)
class DB_ADD_API(APIView):
    def post(self, request):
        id = request.data["id"]
        feature = request.data["feature"] # 对面发什么，这边接受什么
        
        # print(feature[0])
        if len(feature)%512!=0:
            print("feature length invalid")
            return self.success("feature length invalid")
        
        result={"id":id, "flag":sdly_db.add(id, np.array(feature))}
    
        return self.success(result)

class DB_DEL_API_m(APIView):
    def post(self, request):
        ids = request.POST.getlist('ids',[])
        
        result = []
        for i in range(len(ids)):
            feature, flag = sdly_db.delet(ids[i])
            result.append({"id":ids[i], "flag":flag, "feature":feature})
        
        return self.success(result)

class DB_DEL_API(APIView):
    def post(self, request):
        id = request.data["id"]
        feature, flag = sdly_db.delet(id)
        result={"id":id, "flag":flag, "feature":feature}
        
        return self.success(result)

class DB_CHANGE_API_m(APIView):
    def post(self, request):
        ids = request.POST.getlist('ids',[])
        recv = request.POST.getlist('features',[])
        features=[]
        if len(recv)%512!=0:
            return self.success("feature length invalid")
        if len(recv)//512!=len(ids):
            return self.success("feature number does not match with id number")
        for i in range(len(recv)//512):
            features.append([recv[i*512:(i+1)*512]])
        result = []
        for i in range(len(ids)):
            feature, flag = sdly_db.change(ids[i], np.array(features[i]))
            result.append({"id":ids[i], "flag":flag, "feature":feature})
        
        return self.success(result)

class DB_CHANGE_API(APIView):
    def post(self, request):
        id = request.data["id"]
        feature = request.data["feature"]
        # feature = request.POST.getlist('feature',[])
        
        print(feature[0])
        if len(feature)%512!=0:
            print("feature length invalid")
            return self.success("feature length invalid")
        old_feature, flag = sdly_db.change(id, np.array(feature))
        result={"id":id, "flag":flag, "feature":old_feature}
        
        return self.success(result)


class DB_EXISTS_API_m(APIView):
    def post(self, request):
        ids = request.POST.getlist('ids',[])
        
        result = []
        for i in range(len(ids)):
            result.append({"id":ids[i], "flag":sdly_db.exists(ids[i])})
        
        return self.success(result)

class DB_EXISTS_API(APIView):
    def post(self, request):
        id = request.data["id"]
        
        result={"id":id, "flag":sdly_db.exists(id)}
        
        return self.success(result)