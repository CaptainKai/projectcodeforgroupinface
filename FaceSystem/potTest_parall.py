import grequests
import os

url = "http://facecheck.oicp.io:45906/DetectAPI/"
# url = "http://facecheck.oicp.io:45906/DetectVideoAPI/"
file_list_path = "/home/ubuntu/data2/lk/facebox/pytorch_version/FaceBoxes_landmark/data/FDDB/img_list.txt"
pic_root = "/home/ubuntu/data2/lk/facebox/pytorch_version/FaceBoxes_landmark/data/FDDB/images"
file_list = [os.path.join(pic_root, x.strip()+".jpg") for x in open(file_list_path).readlines()]

# file_handler = open('//home/ubuntu/data2/lk/amsoft_pytorch/amsoft_pytorch/data/pairtest/lfw/images/Peggy_McGuinness/Peggy_McGuinness_0001.jpg','rb')
# file_handler = open('//home/ubuntu/data3/lk/pycharm_projects/sdly_video_upload/20201119/152251500758.mp4','rb')
file_handlers = [open(x, 'rb') for x in file_list]

payload={'threshold': '0.5',
'max_length': "1024",
'min_length': "1024",
}

# payload={'detect_threshold': '0.5',
# 'internal': '15'}
# files=[
#   ('file',('152251500758.mp4',file_handler,'application/octet-stream'))
# ]

headers = {}
req_list=[
  grequests.request("POST", url, headers=headers, data=payload,
    files=[
          ('files',('证件照.jpg',file_handlers[i],'image/jpeg'))]
  ) for i in range(5)
]
res_list = grequests.map(req_list)
  
