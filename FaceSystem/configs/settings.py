import os

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))


# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = '9hmm947!pwo41%rk0&vct(jf4^n7(ttp6eyl7&)2v#n^^ltlf#'

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = False

REST_FRAMEWORK = {
    'DEFAULT_AUTHENTICATION_CLASSES': ('rest_framework.authentication.TokenAuthentication',)
}
ALLOWED_HOSTS = ["Access-Control-Allow-Origin", "*"]


# Application definition

INSTALLED_APPS = [
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',


    'rest_framework',
    'checkin',
]

MIDDLEWARE = [
    'django.middleware.security.SecurityMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
]


ROOT_URLCONF = 'configs.urls'

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [os.path.join(BASE_DIR, 'static')]
        ,
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
            ],
        },
    },
]

WSGI_APPLICATION = 'configs.wsgi.application'


AUTH_PASSWORD_VALIDATORS = [
    {
        'NAME': 'django.contrib.auth.password_validation.UserAttributeSimilarityValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.MinimumLengthValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.CommonPasswordValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.NumericPasswordValidator',
    },
]


LANGUAGE_CODE = 'en-us'

TIME_ZONE = 'Asia/Shanghai'

USE_I18N = True

USE_L10N = True

USE_TZ = False


from thirdparty.utils import face_extractor
print("recognition model init done!")
from thirdparty.utils import face_detector, test_file_path, STD, UNIT_SIZE, findNonreflectiveSimilarity
print("detection model init done!")
import cv2
import numpy as np


# redis_host = "114.55.102.254" # TODO redis 数据库的ip
# redis_port = 6379 # TODO redis 数据库的端口号
# redis_password = "loit2019" # TODO redis数据库的密码
# redis_db_index = 3 # TODO redis数据库的编号
from configs.user_settings import redis_host, redis_port, redis_password, redis_db_index
from thirdparty.utils import FeatureDB_redis as RedisClass
# sdly_db = RedisClass(redis_host, redis_port, redis_password, redis_db_index)
sdly_db = RedisClass(redis_host, redis_port, redis_password, redis_db_index)

result_log_path = BASE_DIR+"/running_log" # TODO log文件的存储路径
temp_path = BASE_DIR+"/temp/" # TODO 临时文件夹的名字
if not os.path.exists(temp_path):
    os.mkdir(temp_path)
assert os.path.isdir(temp_path)

import sys
import logging
import logging.handlers

formater = logging.Formatter('%(asctime)s: %(message)s')
logger = logging.getLogger("sdly") # TODO log文件权柄的名字
logger.setLevel(logging.DEBUG)
file_handler = logging.handlers.RotatingFileHandler(result_log_path, mode="a", maxBytes=1024*512, backupCount=5)
file_handler.setFormatter(formater)
file_handler.setLevel(logging.DEBUG)
console_handler = logging.StreamHandler(sys.stdout)
console_handler.formatter = formater
console_handler.setLevel(logging.DEBUG)
logger.addHandler(file_handler)
logger.addHandler(console_handler)