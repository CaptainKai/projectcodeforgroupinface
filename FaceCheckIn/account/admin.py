from django.contrib import admin

# Register your models here.
from django.contrib.auth.models import Group
from account.models import Employer

admin.site.register(Employer)
admin.site.unregister(Group)
