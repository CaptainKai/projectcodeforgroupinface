from django.shortcuts import render
from utils.views import APIView
from account.models import *
from utils.views import tid_maker
from django.contrib import auth
from utils.views import send_register_email

from FaceCheckIn.settings import ip_log_path

# Create your views here.
class RegisterAPI(APIView):
    # 账号注册API
    def post(self, request):
        data = request.data
        # 检验前端传入的数据格式
        # print(data)

        # 检查邮箱和用户名是否被注册
        if Employer.objects.filter(email=data["email"]).exists():
            return self.success("Email has been registered")
        if Employer.objects.filter(username=data["username"]).exists():
            return self.success("username already exists")
        user = Employer.objects.create(username=data["username"], email=data["email"], real_name=data["real_name"], identification=data["identification"])
        user.is_active = False
        user.set_password(data["pwd"])
        user.save()
        send_register_email(data["email"], "register")
        return self.success("Succeeded")


class LoginAPI(APIView):
    # 站好登录API
    def post(self, request):
        data = request.data
        # print(data['username'])
        log = open(ip_log_path, "a+")
        log.write(tid_maker())
        log.write("\n")
        log.write(data['username'])
        log.write("\n")
        log.close()
        user = auth.authenticate(username=data['username'], password=data['pwd'])
        # print(user)
        if user:
            if not user.is_active:
                return self.error("Your account has not been actived!")
            auth.login(request, user)
            return self.success("100")
        else:
            return self.success("400")


class ActiveUserView(APIView):
    # 账号激活API
    def get(self, request):
        active_code = request.GET.get('activecode')
        # print(active_code)
        all_records = EmailVerifyRecord.objects.filter(code=active_code)
        if all_records:
            for record in all_records:
                email = record.email
                # 通过邮箱查找到对应的用户
                user = Employer.objects.get(email=email)
                # 激活用户
                user.is_active = True
                user.save()
        else:
            return self.error("404")
        return render(request, '../static/FrontEnd/page-login.html')# 返回激活成功对应的界面


class SetIPAPI(APIView):
    # 设置白名单API
    def post(self, request):
        username = request.data["id"].upper()
        real_name = request.data["name"]
        place = request.data["place"]
        ip = request.data["ip"]
        ip_record = IPWhiteList.objects.filter(place=place, MasterID=username).first()
        checktime_record = TimeSetting.objects.filter(place=place, MasterID=username).first()
        if not checktime_record:
            checktime_record = TimeSetting.objects.create(place=place, MasterID=username, MasterName=real_name)
            checktime_record.save()
        if ip_record:
            ip_record.ip=ip
            ip_record.place=place
            ip_record.MasterID=username
            ip_record.MasterName=real_name
            ip_record.save()
            return self.success("该地点对应IP已经更新!")
        else:
            IPWhiteList.objects.create(ip=ip, MasterID=username, MasterName=real_name, place=place)
            return self.success("100")


class GetIPsAPI(APIView):
    # 获得白名单列表
    def get(self, request):
        ip_records = IPWhiteList.objects.all()
        if ip_records:
            output = []
            for ip_record in ip_records:
                temp = {}
                temp["ip"] = ip_record.ip
                temp["place"] = ip_record.place
                temp["master"] = ip_record.MasterID+'-'+ip_record.MasterName
                temp["officetime"] = ip_record.OfficeTime
                temp["offtime"] = ip_record.OffTime
                temp["is_active"] = ip_record.time_is_active
                output.append(temp)
            return self.success(output)
        else:
            return self.success("")

    def post(self, request):
        try:
            username = request.data["username"]
        except:
            username = request.data.dict()["_content"]["username"]
        ip_records = IPWhiteList.objects.filter(MasterID=username)
        if ip_records:
            output = []
            for ip_record in ip_records:
                temp = {}
                temp["ip"] = ip_record.ip
                temp["place"] = ip_record.place
                temp["officetime"] = ip_record.OfficeTime
                temp["offtime"] = ip_record.OffTime
                temp["is_active"] = ip_record.time_is_active
                output.append(temp)
            return self.success(output)
        else:
            return self.success("")


class DeleteIPAPI(APIView):
    # 删除白名单API
    def post(self, request):
        username = request.data["id"].upper()
        real_name = request.data["name"]
        # place = request.data["place"]
        ip = request.data["ip"]
        ip_record = IPWhiteList.objects.filter(ip=ip, MasterID=username).first()
        if ip_record:
            ip_record.delete()
        return self.success("100")


class SetCheckTimeAPI(APIView):
    # 设置打卡时间API
    def post(self, request):
        username = request.data["id"].upper()
        real_name = request.data["name"]
        place = request.data["place"]
        OfficeTime = request.data["officetime"]
        OffTime = request.data["offtime"]
        # is_active = request.data["is_active"]
        time_record = TimeSetting.objects.filter(place=place, MasterID=username, MasterName=real_name).first()
        if not time_record:
            return self.success("can not find the place in list")
        time_record = TimeSetting.objects.create(place=place, MasterID=username, MasterName=real_name)
        is_active = True
        time_record.is_active = is_active
        time_record.OfficeTime = OfficeTime
        time_record.OffTime = OffTime
        time_record.save()
        return self.success("该地点对应的打卡时间已经更新!")


class GetCheckTimeAPI(APIView):
    # 获得打卡时间列表
    def get(self, request):
        time_records = TimeSetting.objects.all()
        if time_records:
            output = []
            for time_record in time_records:
                temp = {}
                temp["officetime"] = time_record.OfficeTime
                temp["offtime"] = time_record.OffTime
                temp["place"] = time_record.place
                temp["master"] = time_record.MasterID+'-'+time_record.MasterName
                output.append(temp)
            return self.success(output)
        else:
            return self.success("")

    def post(self, request):
        try:
            username = request.data["username"]
        except:
            username = request.data.dict()["_content"]["username"]
        time_records = TimeSetting.objects.filter(MasterID=username)
        if time_records:
            output = []
            for time_record in time_records:
                temp = {}
                temp["officetime"] = time_record.OfficeTime
                temp["offtime"] = time_record.OffTime
                temp["place"] = time_record.place
                output.append(temp)
            return self.success(output)
        else:
            return self.success("")


class SearchCheckTimeAPI(APIView):
    # 查询打卡时间
    def post(self, request):
        ip = request.data["ip"]
        # place = re
        ip_record = IPWhiteList.objects.filter(ip=ip).first()
        print(ip)
        if ip_record:
            checktime_record = TimeSetting.objects.filter(MasterID=ip_record.MasterID, place=ip_record.place).first()
            # output=[]
            output={}
            output["officetime"]=checktime_record.OfficeTime
            output["offtime"]=checktime_record.OffTime
            output["is_active"]=checktime_record.is_active
            # output.append(temp)
            return self.success(output)
        else:
            return self.success("该IP查询不到其他信息,您可以选择登录设置打卡时间!")
