# Generated by Django 2.0.7 on 2019-02-24 18:39

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('account', '0011_delete_timesetting'),
    ]

    operations = [
        migrations.CreateModel(
            name='TimeSetting',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('MasterID', models.CharField(max_length=50)),
                ('MasterName', models.CharField(max_length=255)),
                ('place', models.CharField(max_length=15)),
                ('is_active', models.BooleanField(default=False, help_text='Designates whether TimeSetting should be treated as active. ')),
                ('OfficeTime', models.TimeField(default='11:00', verbose_name='上班打卡截止时间')),
                ('OffTime', models.TimeField(default='22:00', verbose_name='下班打卡截止时间')),
            ],
        ),
    ]
