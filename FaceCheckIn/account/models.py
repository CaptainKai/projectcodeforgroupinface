from django.db import models
from django.contrib.auth.models import AbstractUser
import django.utils.timezone as timezone


# Create your models here.
class Employer(AbstractUser):
    # 负责人
    real_name = models.CharField(max_length=255, default='佚名')
    identification = models.CharField(max_length=20, default="Professor")

    def __str__(self):
        return "{}".format(self.username)


class EmailVerifyRecord(models.Model):
    # 验证码
    code = models.CharField(max_length=20, verbose_name=u"验证码")
    email = models.EmailField(max_length=50, verbose_name=u"邮箱")
    # 注册验证
    send_type = models.CharField(verbose_name=u"验证码类型", max_length=10, choices=(("register",u"注册"), ("forget",u"找回密码")))
    send_time = models.DateTimeField(verbose_name=u"发送时间", default=timezone.now)

    class Meta:
        verbose_name = u"邮箱验证码"
        verbose_name_plural = verbose_name

    def __unicode__(self):
        return '{0}({1})'.format(self.code, self.email)


class IPWhiteList(models.Model):
    # IP白名单
    ip = models.CharField(max_length=20)
    MasterID = models.CharField(max_length=50)
    MasterName = models.CharField(max_length=255)
    place = models.CharField(max_length=15)
    time_is_active = models.BooleanField(
        default=False,
        help_text=(
            'Designates whether TimeSetting should be treated as active. '
        ),
    )
    OfficeTime = models.TimeField('上班打卡截止时间', default="10:00")
    OffTime = models.TimeField('下班打卡截止时间', default="19:00")


class TimeSetting(models.Model):
    # 打卡时间设置
    MasterID = models.CharField(max_length=50)
    MasterName = models.CharField(max_length=255)
    place = models.CharField(max_length=15)
    is_active = models.BooleanField(
        default=False,
        help_text=(
            'Designates whether TimeSetting should be treated as active. '
        ),
    )
    OfficeTime = models.TimeField('上班打卡截止时间', default="10:00")
    OffTime = models.TimeField('下班打卡截止时间', default="19:00")
