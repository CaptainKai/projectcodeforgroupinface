function getIPs(callback){
	var ip_dups = {};
	//compatibility for firefox and chrome
	var RTCPeerConnection = window.RTCPeerConnection
		|| window.mozRTCPeerConnection
		|| window.webkitRTCPeerConnection;
	var useWebKit = !!window.webkitRTCPeerConnection;

	//bypass naive webrtc blocking using an iframe
	if(!RTCPeerConnection){
		//NOTE: you need to have an iframe in the page right above the script tag
		//
		//<iframe id="iframe" sandbox="allow-same-origin" style="display: none"></iframe>
		//<script>...getIPs called in here...
		//
		var win = iframe.contentWindow;
		RTCPeerConnection = win.RTCPeerConnection
			|| win.mozRTCPeerConnection
			|| win.webkitRTCPeerConnection;
		useWebKit = !!win.webkitRTCPeerConnection;
	}

			//minimal requirements for data connection
	var mediaConstraints = {
		optional: [{RtpDataChannels: true}]
	};

	var servers = {iceServers: [{urls: "stun:stun.services.mozilla.com"}]};

	//construct a new RTCPeerConnection
	var pc = new RTCPeerConnection(servers, mediaConstraints);

	function handleCandidate(candidate){
		//match just the IP address
		var ip_regex = /([0-9]{1,3}(\.[0-9]{1,3}){3}|[a-f0-9]{1,4}(:[a-f0-9]{1,4}){7})/
		var ip_addr = ip_regex.exec(candidate)[0];
		console.log(ip_regex.exec(candidate));
		//remove duplicates
		if(ip_dups[ip_addr] === undefined)
			callback(ip_addr);

		ip_dups[ip_addr] = true;
	}

	//listen for candidate events
	pc.onicecandidate = function(ice){

		//skip non-candidate events
		if(ice.candidate)
			handleCandidate(ice.candidate.candidate);
	};

	//create a bogus data channel
	pc.createDataChannel("");

	//create an offer sdp
	pc.createOffer(function(result){

		//trigger the stun server request
		pc.setLocalDescription(result, function(){}, function(){});

	}, function(){});

	//wait for a while to let everything done
	setTimeout(function(){
		//read candidate info from local description
		var lines = pc.localDescription.sdp.split('\n');

		lines.forEach(function(line){
			if(line.indexOf('a=candidate:') === 0)
				handleCandidate(line);
		});
	}, 1000);
}
function findIP(callback) {
	var myPeerConnection = window.RTCPeerConnection || window.mozRTCPeerConnection || window.webkitRTCPeerConnection; //compatibility for firefox and chrome
	var pc = new myPeerConnection({iceServers: []}),
	noop = function() {},
	localIPs = {},
	ipRegex = /([0-9]{1,3}(\.[0-9]{1,3}){3}|[a-f0-9]{1,4}(:[a-f0-9]{1,4}){7})/g,
	key;
	//console.log("kaishi");
	function ipIterate(ip) {
		if (!localIPs[ip])
			//if(ip.match(ipRegex))
			if(ip.match(/^(192\.168\.|254\.|10\.|172\.(1[6-9]|2\d|3[01]))/))
				callback(ip);
		localIPs[ip] = true;
		console.log(localIPs)
	}
	pc.createDataChannel("");
	pc.createOffer().then(function(sdp) {
		sdp.sdp.split('\n').forEach(function(line) {
		  if (line.indexOf('candidate') < 0) return;
		  line.match(ipRegex).forEach(ipIterate);
		});
		pc.setLocalDescription(sdp, noop, noop);
	});
	pc.onicecandidate = function(ice) {
		if (!ice || !ice.candidate || !ice.candidate.candidate || !ice.candidate.candidate.match(ipRegex)) return;
		ice.candidate.candidate.match(ipRegex).forEach(ipIterate);
		};
	}


