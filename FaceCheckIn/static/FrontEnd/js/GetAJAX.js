var getXHR = (function () {
 //将浏览器支持的AJAX对象放入一个function中，并且根据固定的顺序放到一个队列里。
	var xhrList = [function () {
	 return new XMLHttpRequest
	}, function () {
	 return new ActiveXObject("Microsoft.XMLHTTP");
	}, function () {
	 return new ActiveXObject("Msxml2.XMLHTTP");
	}, function () {
	 return new ActiveXObject("Msxml3.XMLHTTP");
	}], xhr;
	while (xhr = xhrList.shift()) {
	 //此方法的核心，如果当前浏览器支持此对象就用val保存起来
	 // 用保存当前最适合ajax对象的function替换XHR方法，并且结束该循环。
	 // 这样第二次执行XHR方法时就不需要循环，直接就能得到当前浏览器最适ajax对象。
	 // 如果都不支持就抛出自定义引用错误。
	 try {
		 xhr();
		 return xhr;
	 } catch (ex) {

	 }
	}
	throw new ReferenceError("XMLHttpRequest is not supported")
}());
