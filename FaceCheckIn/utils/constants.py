class Choices:
    @classmethod
    def choices(cls):
        d = cls.__dict__
        return [d[item] for item in d.keys() if not item.startswith("__")]

    @classmethod
    def model_choices(cls):
        d = cls.__dict__
        return [(d[item], d[item]) for item in d.keys() if not item.startswith("__")]


class UserType(Choices):
    BOSS = 'boss'
    EMPLOYEE = 'employee'


class FileType(Choices):
    DOC = 'docx/doc'
    EXCEL = 'excel'
    ALL = 'all'
    RAR = 'rar'


class LabPlace(Choices):
    TianGong1 = '天工大厦-A409'
    TianGong2 = '天工大厦-A409(测试机)'
    FangXing = '方兴大厦-1010'
    School = '机电楼-1005'




