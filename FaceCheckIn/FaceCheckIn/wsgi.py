"""
WSGI config for FaceCheckIn project.

It exposes the WSGI callable as a module-level variable named ``application``.

For more information on this file, see
https://docs.djangoproject.com/en/2.0/howto/deployment/wsgi/
"""

import os
import sys
from django.core.wsgi import get_wsgi_application

# sys.path.append('/home/ubuntu/data3/lk/pycharm_projects/FaceCheckIn/venv/bin/')
# sys.path.append('/home/ubuntu/data3/lk/pycharm_projects/FaceCheckIn/venv/lib/python3.5/site-packages')
# sys.path.append('/home/ubuntu/data3/lk/pycharm_projects/FaceCheckIn/venv/lib64/python3.5/site-packages')
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "FaceCheckIn.settings")

application = get_wsgi_application()
