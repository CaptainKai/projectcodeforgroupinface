from PIL import Image
import numpy as np

from torchvision.transforms import functional as F
import torchvision.transforms as transforms
import torch
from torch.autograd import Variable
import torch.backends.cudnn as cudnn

# torch.multiprocessing.set_start_method('spawn')

recogniton_transform = transforms.Compose([
    transforms.ToTensor(),  # range [0, 255] -> [0.0,1.0]
    transforms.Normalize(mean=(0.5, 0.5, 0.5), std=(0.5, 0.5, 0.5))  # range [0.0, 1.0] -> [-1.0,1.0]
])

FACE_RECOGNITION_ROOT="/home/ubuntu/data2/lk/recognition/pytorch"
FACE_DETECTION_ROOT="/home/ubuntu/data2/lk/facebox/pytorch_version/landmark_master"
import sys
sys.path.append(FACE_RECOGNITION_ROOT)
import models

# import pynvml
# pynvml.nvmlInit()
# handle = pynvml.nvmlDeviceGetHandleByIndex(0)# 这里的0是GPU id
# meminfo = pynvml.nvmlDeviceGetMemoryInfo(handle)
from torch.utils.data import DataLoader
import cv2
class FeatureExtractor():
    
    def __init__(self, backbone_name, head_name=None, num_class=10):
        self.backbone = None
        self.head = None
        self.backbone_name = backbone_name
        self.head_name = head_name
        self.num_class = num_class
    
    def load_model(self, backbone_path=None, head_path=None):
        try:
            self.backbone = models.get_model(self.backbone_name)
        except:
            self.backbone = models.get_model(self.backbone_name, input_size=[112,112])
        if self.head_name is not None:
            self.head = models.get_model(self.backbone_name, in_features=512, out_features=self.num_class)
        if backbone_path is None and head_path is None:
            print("at least one argument!")
            exit(0)
        if backbone_path is not None and self.backbone is not None:
            self.backbone.load_state_dict(torch.load(backbone_path))
            self.backbone.eval()
        if head_path is not None and self.head is not None:
            self.head.load_state_dict(torch.load(head_path))
            self.head.eval()

    def extractDeepFeature(self, img, flop=False, transform=recogniton_transform):
        '''

        :param img:
        :param flop:
        :param transform:
        :return: array
        '''
        if img.size[0] != 112 or img.size[1] != 112:
            print("resizing")
            img = img.resize((112, 112))
        img = transform(img)
        if flop:
            img_ = transform(F.hflip(img))
            img_ = img_.unsqueeze(0)
            img = img.unsqueeze(0)
            feature = torch.cat((self.backbone(img), self.backbone(img_)), 1)[0]   
        else:
            img = img.unsqueeze(0)
            feature = self.backbone(img)[0].detach().numpy()
        
        feature = feature/cv2.norm(feature)
        return feature
    
    def extractDeepFeatures(self, imgs, transform=recogniton_transform):
        '''

        :param imgs:
        :param transform:
        :return: [array, array]
        '''
        if len(imgs) == 0:
            return []
        img_list=[]
        for img in imgs:
            if img.size[0] != 112 or img.size[1] != 112:
                print("resizing")
                img = img.resize((112, 112))
            img = transform(img)
            img_list.extend(img.unsqueeze(0))
        # print(len(imgs))
        dl = DataLoader(img_list, batch_size=len(imgs), shuffle=False)
        result=[]
        for images in dl:
            features = extractor_object.backbone(images)
            for feature in features:
                feature = feature.detach().numpy()
                feature = feature/cv2.norm(feature)
                result.append(feature)
        return result


# print(meminfo.used)
recognition_model_name = "SimpleResnet_20"
# model_path = "/home/ubuntu/data2/lk/amsoft_pytorch/amsoft_pytorch/snapshot/msra-gt-mask-arguementation-0.5-resnest50_test/amsoft_25_checkpoint.pth"
# model_path = "/home/ubuntu/data2/lk/recognition/pytorch/snapshot/ArcfaceResnet_50_base/backbone_38_checkpoint.pth"
recognition_model_path = "/home/ubuntu/data2/lk/recognition/pytorch/snapshot/SimpleResnet_20_base/amsoft_33_checkpoint.pth"


multi_gpus = False
sys.path.append(FACE_DETECTION_ROOT)
from data import cfg

from layers.functions.prior_box import PriorBox
from checkin.utils.nms_wrapper import nms
#from utils.nms.py_cpu_nms import py_cpu_nms
import cv2
from faceboxes_models.faceboxes import FaceBoxes

from checkin.utils.box_utils import decode
# from checkin.utils.box_utils import boxes2face
# from checkin.utils.box_utils import Proposal_filter
# from checkin.utils.nme import NME
from checkin.utils.model_handle import load_model
import time


class Face_Detector():
    def __init__(self, top_k=5000, \
                nms_threshold=0.3, keep_top_k=750, cpu=False, resize=2.5):
        # self.confidence_threshold=confidence_threshold
        self.top_k = top_k
        self.nms_threshold = nms_threshold
        self.keep_top_k = keep_top_k
        # self.vis_thres = vis_thres
        # self.device = torch.device("cpu" if cpu else "cuda")
        self.device = torch.device("cpu")
        self.cpu = cpu
        self.resize = resize
    
    def load_model(self, model_path):

        self.net = FaceBoxes(phase='test', size=None, num_classes=3)    # initialize detector
        self.net = load_model(self.net, model_path, False)
        self.net.eval()
        # self.net = self.net.to(self.device)

    def detect(self, img, threshold=0.5):
        # torch.cuda.set_device(0)
        # os.environ["CUDA_VISIBLE_DEVICES"]="0"
        # self.net = self.net.to(self.device)
        img = np.float32(img)
        img = cv2.resize(img, None, None, fx=self.resize, fy=self.resize, interpolation=cv2.INTER_LINEAR)
        im_height, im_width, _ = img.shape
        scale = torch.Tensor([img.shape[1], img.shape[0], img.shape[1], img.shape[0]])
        scale_landm = torch.Tensor([img.shape[1], img.shape[0], img.shape[1], img.shape[0], img.shape[1],
                                    img.shape[0], img.shape[1], img.shape[0], img.shape[1], img.shape[0]])
        img -= (104, 117, 123)
        img = img.transpose(2, 0, 1)
        img = torch.from_numpy(img).unsqueeze(0)

        img = img.to(self.device)
        scale = scale.to(self.device)
        scale_landm = scale_landm.to(self.device)

        # loc, conf = self.net(img)  # forward pass # TODO
        loc, conf, landm = self.net(img)  # forward pass # TODO

        priorbox = PriorBox(cfg, image_size=(im_height, im_width))
        priors = priorbox.forward()
        priors = priors.to(self.device)
        prior_data = priors.data
        boxes = decode(loc.data.squeeze(0), prior_data, cfg['variance'])
        boxes = boxes * scale / self.resize
        boxes = boxes.cpu().numpy()

        num_priors = prior_data.shape[0]
        landm = landm.data.squeeze(0)*cfg['variance'][0]
        landm[:, 0:9:2] = (landm[:, 0:9:2]*prior_data[:, 2].unsqueeze(1).expand([num_priors, 5])+prior_data[:, 0].unsqueeze(1).expand([num_priors, 5]))
        # landm[:, 0:9:2] = (landm[:, 0:9:2]+prior_data[:, 0].unsqueeze(1).expand([num_priors, 5]))
        landm[:, 1:10:2] = (landm[:, 1:10:2]*prior_data[:, 3].unsqueeze(1).expand([num_priors, 5])+prior_data[:, 1].unsqueeze(1).expand([num_priors, 5]))
        # landm[:, 1:10:2] = (landm[:, 1:10:2]+prior_data[:, 1].unsqueeze(1).expand([num_priors, 5]))
        landm = landm * scale_landm / self.resize
        landm = landm.cpu().numpy()

        # print(conf.squeeze(0).data.cpu().numpy().shape)
        scores = conf.squeeze(0).data.cpu().numpy().max(axis=1)
        label_index = np.argmax(conf.squeeze(0).data.cpu().numpy(), axis=1)
        # print(label_index[:5])
        # print(scores[:5])

        no_background_mask = label_index>0
        scores = scores[no_background_mask]
        boxes = boxes[no_background_mask]
        landm = landm[no_background_mask]
        label_index = label_index[no_background_mask]

        # ignore low scores
        inds = np.where(scores > threshold)[0]
        boxes = boxes[inds]
        landm = landm[inds]
        scores = scores[inds]
        label_index = label_index[inds]
        # print(label_index[:5])


        # keep top-K before NMS
        order = scores.argsort()[::-1][:self.top_k]
        boxes = boxes[order]
        landm = landm[order]
        scores = scores[order]
        label_index = label_index[order]

        # do NMS
        # dets = np.hstack((boxes, scores[:, np.newaxis])).astype(np.float32, copy=False)
        dets = np.hstack((boxes, scores[:, np.newaxis], landm, label_index[:, np.newaxis])).astype(np.float32, copy=False)
        #keep = py_cpu_nms(dets, self.nms_threshold)
        keep = nms(dets[:,:5], self.nms_threshold, force_cpu=True)
        dets = dets[keep, :]

        # keep top-K faster NMS
        dets = dets[:self.keep_top_k, :]

        return dets

    def detect_video(self, frame_list, threshold=0.5):
        frame_list_tensor = []
        for idx, frame in enumerate(frame_list):
            frame = np.float32(frame)
            frame = cv2.resize(frame, None, None, fx=self.resize, fy=self.resize, interpolation=cv2.INTER_LINEAR)
            if idx==0:
                im_height, im_width, _ = frame.shape
                scale = torch.Tensor([frame.shape[1], frame.shape[0], frame.shape[1], frame.shape[0]])
                scale_landm = torch.Tensor([frame.shape[1], frame.shape[0], frame.shape[1], frame.shape[0], frame.shape[1],
                                            frame.shape[0], frame.shape[1], frame.shape[0], frame.shape[1], frame.shape[0]])
            frame -= (104, 117, 123)
            frame = frame.transpose(2, 0, 1)
            frame = torch.from_numpy(frame).unsqueeze(0)
            frame_list_tensor.extend(frame)
        dl = DataLoader(frame_list_tensor, batch_size=len(frame_list_tensor), shuffle=False)
        # loc, conf = self.net(img)  # forward pass # TODO
        for img_list in dl:
            img_list = img_list.to(self.device)
            loc_list, conf_list, landm_list = self.net(img_list)  # forward pass # TODO

        priorbox = PriorBox(cfg, image_size=(im_height, im_width))
        priors = priorbox.forward()
        priors = priors.to(self.device)
        prior_data = priors.data

        dets_list = []
        t0 = time.time()
        for loc, conf, landm in zip(loc_list, conf_list, landm_list):
            boxes = decode(loc.data.squeeze(0), prior_data, cfg['variance'])
            boxes = boxes * scale / self.resize
            boxes = boxes.cpu().numpy()

            num_priors = prior_data.shape[0]
            landm = landm.data.squeeze(0)*cfg['variance'][0]
            landm[:, 0:9:2] = (landm[:, 0:9:2]*prior_data[:, 2].unsqueeze(1).expand([num_priors, 5])+prior_data[:, 0].unsqueeze(1).expand([num_priors, 5]))
            # landm[:, 0:9:2] = (landm[:, 0:9:2]+prior_data[:, 0].unsqueeze(1).expand([num_priors, 5]))
            landm[:, 1:10:2] = (landm[:, 1:10:2]*prior_data[:, 3].unsqueeze(1).expand([num_priors, 5])+prior_data[:, 1].unsqueeze(1).expand([num_priors, 5]))
            # landm[:, 1:10:2] = (landm[:, 1:10:2]+prior_data[:, 1].unsqueeze(1).expand([num_priors, 5]))
            landm = landm * scale_landm / self.resize
            landm = landm.cpu().numpy()

            # print(conf.squeeze(0).data.cpu().numpy().shape)
            scores = conf.squeeze(0).data.cpu().numpy().max(axis=1)
            label_index = np.argmax(conf.squeeze(0).data.cpu().numpy(), axis=1)
            # print(label_index[:5])
            # print(scores[:5])

            no_background_mask = label_index>0
            scores = scores[no_background_mask]
            boxes = boxes[no_background_mask]
            landm = landm[no_background_mask]
            label_index = label_index[no_background_mask]

            # ignore low scores
            inds = np.where(scores > threshold)[0]
            boxes = boxes[inds]
            landm = landm[inds]
            scores = scores[inds]
            label_index = label_index[inds]
            # print(label_index[:5])


            # keep top-K before NMS
            order = scores.argsort()[::-1][:self.top_k]
            boxes = boxes[order]
            landm = landm[order]
            scores = scores[order]
            label_index = label_index[order]

            # do NMS
            # dets = np.hstack((boxes, scores[:, np.newaxis])).astype(np.float32, copy=False)
            dets = np.hstack((boxes, scores[:, np.newaxis], landm, label_index[:, np.newaxis])).astype(np.float32, copy=False)
            #keep = py_cpu_nms(dets, self.nms_threshold)
            keep = nms(dets[:,:5], self.nms_threshold, force_cpu=True)
            dets = dets[keep, :]

            # keep top-K faster NMS
            dets = dets[:self.keep_top_k, :]

            if dets is None:
                dets=[]

            dets_list.append(dets)
        t1 = time.time()
        print("vedio detect: ", t1-t0)
        return dets_list
# from torch.processing
import queue


class QueueObject():
 
    def __init__(self, queue, auto_get=False):
        self._queue = queue
        self.object = self._queue.get() if auto_get else None
 
    def __enter__(self):
        if self.object is None:
            self.object = self._queue.get()
            print("instance one obj")
        return self.object
 
    def __exit__(self, Type, value, traceback):
        print("exit")
        if self.object is not None:
            self._queue.put(self.object)
            self.object = None
 
    def __del__(self):
        print("del")
        if self.object is not None:
            self._queue.put(self.object)
            self.object = None  

import torch.multiprocessing as mp

# mp.set_start_method("spawn") # RuntimeError: context has already been set
# detecor_queue = mp.Queue()
# for i in range(1):
#     face_detector = Face_Detector()
#     face_detector.load_model(FACE_DETECTION_ROOT+"/weights/mask_landmark_augue/Final_FaceBoxes.pth")
#     detecor_queue.put(face_detector)
# detector_object = QueueObject(detecor_queue)
face_detector = Face_Detector()
face_detector.load_model(FACE_DETECTION_ROOT+"/weights/mask_landmark_augue/Final_FaceBoxes.pth")
detector_object = face_detector
print("detection model init done!")   
# print(meminfo.used)
# face_extractor_queue = mp.Queue()
# for i in range(1):

#     face_extractor = FeatureExtractor(recognition_model_name)
#     face_extractor.load_model(recognition_model_path)
#     # device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')
#     face_extractor.backbone.eval()

#     face_extractor_queue.put(face_extractor)
# extractor_object = QueueObject(face_extractor_queue)
face_extractor = FeatureExtractor(recognition_model_name)
face_extractor.load_model(recognition_model_path)
# device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')
extractor_object = face_extractor

print("recognition model init done!")
# print(meminfo.used)

STD = [39.2946, 69.5318, 62.0252, 29.5493, 65.7200, \
    21.6963, 14.5014, 56.7366, 87.3655, 80.2041]
UNIT_SIZE = 112


def findNonreflectiveSimilarity(src,dst):
    p=src
    A=np.zeros(40)
    for i in range(5):
        j =i*4
        A[j]=p[i]
        A[j+21]=p[i]*(-1)
        A[j+1]=A[j+20]=p[i+5]
        A[j+2] = A[j+23] = 1
        A[j+3] = A[j+22] = 0
    B=np.reshape(A,(10,4))
    # print(B)
    B=np.mat(B)
    A=B.I
    # print(A)
    A=np.array(A)
    M=np.dot(A,dst)

    M=np.array(M)
    m=np.zeros(6)

    m[0]=M[0]
    m[3]=M[1]*(-1)
    m[1]=M[1]
    m[2]=M[2]
    m[4]=M[0]
    m[5]=M[3]
    I=np.mat(np.reshape(m,(2,3)))
    
    # print(I)
    return I

import os
import numpy as np
import struct


class FeatureDB():
    def __init__(self, db_root, db_list):
        self.root_path = db_root
        self.list_path = db_list
        if not os.path.exists(self.list_path):
            with open(self.list_path, "w") as f:
                f.close()
        self.lock = False
        self.db = []
        self.load_db()

    def load_db(self):
        list_file = open(self.list_path, "r")
        self.list = [x.strip() for x in list_file.readlines()]
        list_file.close()
        self.lock = True
        for id in self.list:
            feature_path = os.path.join(self.root_path, id+".bin")
            feature = np.float32(np.fromfile(feature_path,dtype=np.float32))[4:]
            # feature = np.float32(struct.unpack_from())[4:]
            self.db.append(feature/cv2.norm(feature))
        self.lock = False
    
    def search(self, id):
        while True:
            if not self.lock:
                if id in self.list:
                    return True
                else:
                    return False
            print("waiting")

    def add(self, id, feature):
        if self.search(id):
            print("exists!")
            return False
        while True:
            if not self.lock:
                self.lock=True
                self.list.append(id)
                self.db.append(feature)

                dest_path = os.path.join(self.root_path, id+".bin")
                dest_file = open(dest_path,'wb')
                dest_file.write(struct.pack('4i', len(feature), 1, 4, 5))
                dest_file.write(struct.pack('%df'%len(feature), *feature))
                dest_file.close()

                list_file = open(self.list_path, "a+")
                list_file.write(id+"\n")
                list_file.close()
                self.lock=False
                return True
            print("waiting")

    def delet(self, id):
        if not self.search(id):
            return False
        while True:
            if not self.lock:
                self.lock=True
                id_index = self.list.index(id)
                self.list.remove(id)
                self.db.pop(id_index)

                dest_path = os.path.join(self.root_path, id+".bin")
                os.remove(dest_path)

                list_file = open(self.list_path, "w")
                for id in self.list:
                    list_file.write(id+"\n")
                list_file.close()

                self.lock=False
                return True
            print("waiting")

    def change(self, id, feature):
        if not self.search(id):
            return False
        while True:
            if not self.lock:
                self.lock=True
                id_index = self.list.index(id)
                self.db[id_index]=feature

                dest_path = os.path.join(self.root_path, id+".bin")
                dest_file = open(dest_path,'wb')
                dest_file.write(struct.pack('4i', len(feature), 1, 4, 5))
                dest_file.write(struct.pack('%df'%len(feature), *feature))
                dest_file.close()

                self.lock=False
                return True
            print("waiting")

    def compare(self, feature, rank_N=1):
        while True:
            if not self.lock:
                self.lock=True
                if len(self.list)==0 or len(self.list)<rank_N:
                    return ["db is too small"]
                score = np.dot(np.array(self.db), feature.transpose())
                self.lock=False
                score_index_sorted = np.argsort(score, axis=0) # db_length*feature_num
                expected_index = score_index_sorted[-rank_N:][:]
                result=[]
                for i in range(feature.shape[0]):
                    result.append([])
                    for j in range(rank_N):
                        result_index = expected_index[j][i]
                        result[-1].append([self.list[result_index], score[result_index, i]])
                return result
            print("waiting")


face_db = FeatureDB(db_root="/home/ubuntu/data3/lk/pycharm_projects/face_db", \
                    db_list="/home/ubuntu/data3/lk/pycharm_projects/face_db/id_list")

import redis

sdly_redis_host = "114.55.102.254"
sdly_redis_port = 6379
sdly_redis_password = "loit2019"
sdly_redis_db_index = 3


class RedisClass():
    def __init__(self, host=sdly_redis_host, port=sdly_redis_port, \
                password=sdly_redis_password, db_index=sdly_redis_db_index):
        self.host = host
        self.port = port
        self.password = password
        self.db_index = db_index
        self.pool = redis.ConnectionPool(host=self.host, port=self.port, password=self.password, db=self.db_index)
        self.r = redis.StrictRedis(connection_pool=self.pool)
        self.key_list = []
        self.feature_list = []

    def get_all(self):
        '''
        :return: [id, id...],[array, array]
        '''
        # print(self.r)
        pipe = self.r.pipeline()
        self.key_list = []
        self.key_list_temp = []
        keys = self.r.keys()
        # print(keys)
        for key in keys:
            pipe.lrange(key, 0, -1)
            self.key_list_temp.append(key)
        self.feature_list = []
        for (k, v) in zip(self.key_list_temp, pipe.execute()):
            if len(v) != 512:
                print("the value for %s is invalid"%(k))
                continue
            self.key_list.append(k.decode())
            self.feature_list.append(np.array(v, dtype='float32'))
        # print(self.key_list)
        # return self.key_list, self.feature_list

    def compare(self, features, rankn=1):
        '''
        :param features: [array, array]
        :param rankn:
        :return: [[[id, score]...]]
        '''
        self.get_all()
        result = []
        if len(features) == 0:
            return result
        # print(self.key_list)

        if len(self.key_list) < rankn:
            print("there is no enough data for compare")
            for i in range(len(features)):
                result.append([])
                for j in range(rankn):
                    result[-1].append([0, 0])
            return result
        score = np.dot(np.array(self.feature_list), np.array(features).transpose())
        score = np.absolute(score)
        score_index_sorted = np.argsort(score, axis=0) # db_length*feature_num
        expected_index = score_index_sorted[-rankn:][:]
        for i in range(len(features)):
            result.append([])
            for j in range(rankn):
                result_index = expected_index[j][i]
                result[-1].append([self.key_list[result_index], score[result_index, i]])
        return result


sdly_db = RedisClass()

import logging


formater = logging.Formatter('%(asctime)s: %(message)s')
result_log_path = "/home/ubuntu/data3/lk/pycharm_projects/FaceCheckIn/running_log"

logger = logging.getLogger("facecheckin")
logger.setLevel(logging.DEBUG)
file_handler = logging.handlers.RotatingFileHandler(result_log_path, mode="a", maxBytes=1024*1024)
file_handler.setFormatter(formater)
file_handler.setLevel(logging.DEBUG)
console_handler = logging.StreamHandler(sys.stdout)
console_handler.formatter=formater
console_handler.setLevel(logging.DEBUG)
logger.addHandler(file_handler)
logger.addHandler(console_handler)

sdly_result_log_path = "/home/ubuntu/data3/lk/pycharm_projects/FaceCheckIn/sdly_running_log"

sdly_logger = logging.getLogger("sdly")
sdly_logger.setLevel(logging.DEBUG)
file_handler = logging.handlers.RotatingFileHandler(sdly_result_log_path, mode="a", maxBytes=1024*512, backupCount=5)
file_handler.setFormatter(formater)
file_handler.setLevel(logging.DEBUG)
console_handler = logging.StreamHandler(sys.stdout)
console_handler.formatter = formater
console_handler.setLevel(logging.DEBUG)
sdly_logger.addHandler(file_handler)
sdly_logger.addHandler(console_handler)

print(torch.__version__)


def cluster(feature_list, threshold):
    if len(feature_list)==0:
        return []
    id_list = [-1]*len(feature_list)
    id_list[-1] = 0
    id_record = 0
    score = np.dot(np.array(feature_list), np.array(feature_list).transpose())
    score = np.absolute(score)
    # expected_index = score_index_sorted[-1:][:]
    id_queue = [i for i in range(len(feature_list))]
    id_index = id_queue.pop()
    while len(id_queue)!=0:
        score_list = score[:][id_index]
        invalid_index = np.where(score_list>=threshold)
        for index in invalid_index[0]:
            if index!=id_index and id_list[index]==-1:
                id_list[index] = id_record
                id_queue.append(index)
        id_index = id_queue.pop()
        if id_list[id_index]==-1:
            id_record +=1
            id_list[id_index]=id_record
    return id_list

