"""FaceCheckIn URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path
from checkin.views import *
from account.views import *

urlpatterns = [
    path('admin/', admin.site.urls),
    # path('TestAPI/', TestAPI.as_view()),
    path('RegisterAPI/', RegisterFaceAPI.as_view()),
    path('LoginAPI/', LoginAPI.as_view()),
    path('CheckInAPI/', CheckIn_API.as_view()),
    # path('CheckIn_opt_API/', CheckIn_opt_API.as_view()),
    # path('CheckIn_Peach_API/', CheckIn_Peach_API.as_view()),
    # path('RegisterFaceAPI/', RegisterFaceAPI.as_view()),
    # path('RegisterFace_Peach_API/', RegisterFace_Peach_API.as_view()),
    path('GetAllAPI/', GetAllAPI.as_view()),
    path('active/', ActiveUserView.as_view()),
    path('SetIPAPI/', SetIPAPI.as_view()),
    path('GetIPsAPI/', GetIPsAPI.as_view()),
    path('DeleteIPAPI/', DeleteIPAPI.as_view()),
    path('SetCheckTimeAPI/', SetCheckTimeAPI.as_view()),
    path('GetCheckTimeAPI/', GetCheckTimeAPI.as_view()),
    path('SearchCheckTimeAPI/', SearchCheckTimeAPI.as_view()),
    path('ExtractFeatureAPI/', ExtractFeatureAPI.as_view()),
    # path('ExtractVideoFeatureAPI/', ExtractVideoFeatureAPI.as_view()),
    path('DetectVideoAPI/', DetectVideoAPI.as_view()),
    path('DetectAPI/', DetectAPI.as_view()),
]
