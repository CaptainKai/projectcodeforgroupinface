from django.db import models
import django.utils.timezone as timezone
from utils.constants import LabPlace
from account.models import *
# Create your models here.


class Employees(models.Model):
    # 员工
    username = models.CharField(max_length=10)
    img_url = models.CharField(max_length=255)
    real_name = models.CharField(max_length=15, default='佚名')
    lab = models.CharField(max_length=15, default=LabPlace.School)
    superior = models.CharField(max_length=15, default="0001")
    is_effective = models.BooleanField(default=True)

    def __str__(self):
        return "{}".format(self.username+"-"+self.real_name)


class Records(models.Model):
    # 签到记录
    time = models.DateTimeField('签到日期', default=timezone.now)
    person_username = models.CharField(max_length=15, default="")
    person_real_name = models.CharField(max_length=15, default="")
    person_place = models.CharField(max_length=15, default=LabPlace.School)
    person_superior = models.CharField(max_length=15, default="0001")
    img_url = models.CharField(max_length=255)
    is_effective = models.BooleanField(default=True)

    def __str__(self):
        return "{}".format(self.id)


class PeachFarmers(models.Model):
    # 员工
    phone = models.CharField(max_length=12)
    img_url = models.CharField(max_length=255)
    real_name = models.CharField(max_length=15, default='佚名')
    address = models.CharField(max_length=45)
    sex = models.CharField(max_length=1)
    is_effective = models.BooleanField(default=True)

    def __str__(self):
        return "{}".format(self.phone+"-"+self.real_name)


class PeachRecords(models.Model):
    # 签到记录
    time = models.DateTimeField('签到日期', default=timezone.now)
    person_phone = models.CharField(max_length=15, default="")
    person_real_name = models.CharField(max_length=15, default="")
    person_peach = models.CharField(max_length=15, default="")
    img_url = models.CharField(max_length=255)
    is_effective = models.BooleanField(default=True)

    def __str__(self):
        return "{}".format(self.person_phone + "-" + self.person_real_name)