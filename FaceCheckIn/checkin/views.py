from utils.views import APIView, tid_maker
from FaceCheckIn.settings import register_pic_db, trash_path, check_pic_record
from FaceCheckIn.settings import sdly_register_pic_db

from checkin.models import *
import os
import shutil

from FaceCheckIn.settings import STD, UNIT_SIZE, store_suffix
from FaceCheckIn.settings import extractor_object, detector_object, face_db, logger, sdly_logger

from FaceCheckIn.settings import findNonreflectiveSimilarity
from PIL import Image
import cv2
import numpy as np

import traceback
import time

class RegisterFaceAPI(APIView):
    # 人脸注册API
    def post(self, request):
        data = request.FILES.get('file').read()
        username = request.data["id"].upper()
        real_name = request.data["name"]
        place = request.data["place"]
        master = request.data["master"]

        file_name = username + '-' + real_name

        result = {}
        pil_img = Image.open(request.FILES.get('file'))
        cv_img = cv2.cvtColor(np.asarray(pil_img), cv2.COLOR_RGB2BGR)
        # with detector_object as dob:
        #     dets = dob.detect(cv_img)
        try:
            dets = detector_object.detect(cv_img)
        except:
            traceback.print_exc()
            print("detect error!")
            return self.success("Internet Error")

        if dets.shape[0] == 0:
            result["result"] = ["200"]
            return self.success(result)
        elif dets.shape[0] > 1:
            result["result"] = ["300"]
            return self.success(result)
        else:
            result["result"] = ["100"]
            if dets[0, -1]==2:
                file_name = file_name + "_mask"
            img_url = register_pic_db+"/"+file_name + store_suffix
            if os.path.exists(img_url):
                save_name = tid_maker()+"-"+file_name + store_suffix
                shutil.copyfile(img_url, trash_path+"/"+save_name)
            cv2.imwrite(img_url, cv_img)

            landmark = dets[0, 5:15]
            src = np.zeros(10)  # [x1,y1,...]=>[x1,x2...y1,y2...]
            for i in range(5):
                src[i] = landmark[i*2]
            for i in range(5):
                src[i+5] = landmark[i*2+1]
            M = findNonreflectiveSimilarity(src, STD)
            res = cv2.warpAffine(cv_img, M, (int(UNIT_SIZE), int(UNIT_SIZE)))
            pil_res = Image.fromarray(cv2.cvtColor(res, cv2.COLOR_BGR2RGB))

            # with extractor_object as eob:
            #     feature = eob.extractDeepFeature(pil_res)
            try:
                feature = extractor_object.extractDeepFeature(pil_res)
            except:
                traceback.print_exc()
                print("extract error!")
                return self.success("Internet Error")
            feature = feature/cv2.norm(feature)
            try:
                if face_db.search(file_name):
                    face_db.change(file_name, feature)
                else:
                    if not face_db.add(file_name, feature):
                        return self.success("failed")
            except:
                traceback.print_exc()
                print("face db error!")
                self.success("please contact manager: db error!")

            logger.info(" ".join([username, real_name, place, master]))

            if not Employees.objects.filter(username=username, real_name=real_name):
                if Employer.objects.filter(username=master.split("-")[0], real_name=master.split("-")[1]).filter():
                    employer = Employer.objects.filter(username=master.split("-")[0], real_name=master.split("-")[1]).filter().first()
                    employee = Employees.objects.create(username=username, real_name=real_name, img_url=img_url, lab=place, superior=employer.username, is_effective=True)
                    employee.save()
                    print("new!")
                    return self.success(result)
            else:
                person = Employees.objects.filter(username=username, real_name=real_name).first()
                person.img_url = img_url
                person.lab = place
                employer = Employer.objects.filter(username=master.split("-")[0], real_name=master.split("-")[1]).filter().first()
                person.superior = employer.username
                person.save()
                print("exist!")
                return self.success("更新成功")


from FaceCheckIn.settings import sdly_db, sdly_video_root


class ExtractFeatureAPI(APIView):
    # 人脸特征提取API
    def post(self, request):
        # print(request)
        # print(request.FILES.getlist("files"))
        req_files = request.FILES.getlist("files")
        if len(req_files)==0:
            print("no file get, please check the key you send")
            result = [{"feature":[], "picked_id":"None", "exists": 0}]
            return self.success(result)
        threshold = float(request.data["threshold"])
        sdly_logger.info("threshold %f"%( threshold ))
        img_list = []
        file_name_list = []
        for req_file in req_files:
            time_id = tid_maker()
            temp_file_name = os.path.join(sdly_register_pic_db, time_id + '.png')
            pil_img = Image.open(req_file).convert("RGB")
            pil_img.save(temp_file_name)
            file_name_list.append(temp_file_name)
            sdly_logger.info("get data from sdly")
            img_list.append(pil_img)
        sdly_logger.info(str(file_name_list))
        try:
            features = extractor_object.extractDeepFeatures(img_list)
            # print(feature)
        except:
            traceback.print_exc()
            sdly_logger.info("extract error!")
            return self.success("Internet Error")
        # print(feature)
        result = []
        try:
            com_result = sdly_db.compare(features)
        except:
            traceback.print_exc()
            sdly_logger.info("db read error!")
            return self.success("Redis Manipulation Error")
        try:
            for i in range(len(features)):
                result_dict = {}
                result_dict["feature"] = features[i]
                if com_result[i][0][1] >= threshold:
                    result_dict["picked_id"] = str(com_result[i][0][0])
                    result_dict["exists"] = 1
                else:
                    result_dict["picked_id"] = "None"
                    result_dict["exists"] = 0
                result.append(result_dict)
        except:
            traceback.print_exc()
            sdly_logger.info("compare Error")
            return self.success("compare Error")
        sdly_logger.info(str([x["picked_id"] for x in result]))
        return self.success(result)


from utils.views import img2base64


class ExtractVideoFeatureAPI(APIView):
    # lack of repeat
    def post(self, request):
        # print(request)
        # print(request.FILES.getlist("files"))
        req_file = request.FILES.get("file")
        detect_threshold = float(request.data["detect_threshold"])
        recog_threshold = float(request.data["recog_threshold"])
        internal = int(request.data["internal"])

        sdly_logger.info("detect_threshold %f" % detect_threshold)
        sdly_logger.info("recog_threshold %f" % recog_threshold)
        sdly_logger.info("internal %d" % internal)

        time_id = tid_maker()
        file_url = os.path.join(sdly_video_root, time_id[:8])

        # 创建年月日文件夹
        if not os.path.exists(file_url):
            try:
                os.mkdir(file_url)
            except:
                sdly_logger.info("服务器空间不够!请联系管理员!")
                return self.success("服务器空间不够!请联系管理员!")
        destination_path = os.path.join(file_url, time_id[8:]+".mp4")
        destination = open(destination_path, 'wb+')
        for chunk in req_file.chunks():
            destination.write(chunk)
        destination.close()
        sdly_logger.info("get vedio data from sdly")
        video_reader = cv2.VideoCapture(destination_path)
        index = 0
        frame_list = []
        while True:
            success, frame = video_reader.read()
            # frame = cv2.transpose(frame)
            if not success:
                sdly_logger.info("video read done")
                break
            if index%internal==0:
                frame_list.append(frame)
            #         frame_list.append(frame)
            index += 1
        sdly_logger.info("get %d pics" % len(frame_list))
        result = {"get_pic_num": len(frame_list)}
        final_com_results = []

        dets_list = detector_object.detect_video(frame_list, threshold=detect_threshold)
        extractor_list = []
        for dets, frame in zip(dets_list, frame_list):
            final_com_results.append([frame])
            final_com_results[-1].append([])
            for det in dets:
                landmark = det[5:15]
                src = np.zeros(10)  # [x1,y1,...]=>[x1,x2...y1,y2...]
                for i in range(5):
                    src[i] = landmark[i*2]
                for i in range(5):
                    src[i+5] = landmark[i*2+1]
                M = findNonreflectiveSimilarity(src, STD)
                cv_res = cv2.warpAffine(frame, M, (int(UNIT_SIZE), int(UNIT_SIZE)))
                pil_res = Image.fromarray(cv2.cvtColor(cv_res, cv2.COLOR_BGR2RGB))
                final_com_results[-1][1].append(cv_res)
                extractor_list.append(pil_res)
        try:
            features = extractor_object.extractDeepFeatures(extractor_list)
            # print(feature)
        except:
            traceback.print_exc()
            sdly_logger.info("extract error!")
            return self.success("Internet Error")
        # print(feature)
        sdly_logger.info("video extract done")
        try:
            com_result = sdly_db.compare(features)
        except:
            traceback.print_exc()
            sdly_logger.info("db read error!")
            return self.success("Redis Manipulation Error")
        sdly_logger.info("video compare done")
        try:
            feature_index=0
            for frame_index, dets in enumerate(dets_list):
                for det_index, det in enumerate(dets):
                    com_result[feature_index][0].append(frame_index)
                    com_result[feature_index][0].append(det_index)
                    com_result[feature_index][0].append(feature_index)
                    feature_index+=1

            result_sorted_score = sorted(com_result, key=lambda x:x[0][1], reverse=True)
            result_sorted_key = sorted(result_sorted_score, key=lambda x:x[0][0])

            old_result = result_sorted_key[0]
            for singleresult in result_sorted_key[1:]:
                old_key = old_result[0][0]
                new_key = singleresult[0][0]
                if new_key!=old_key:
                    frame_index = old_result[0][2]
                    det_index = old_result[0][3]
                    features_index = old_result[0][4]
                    info = dets_list[frame_index][det_index]
                    det_frame = final_com_results[frame_index][1][det_index]
                    feature = features[features_index]
                    score = old_result[0][1]
                    id = old_result[0][0]
                    # if score>=threshold:
                    final_com_results[frame_index].append([info, feature, id, score, det_frame])
                    old_result = singleresult
                else:
                    continue

            frame_index = old_result[0][2]
            det_index = old_result[0][3]
            features_index = old_result[0][4]
            info = dets_list[frame_index][det_index]
            det_frame = final_com_results[frame_index][1][det_index]
            feature = features[features_index]
            score = old_result[0][1]
            id = old_result[0][0]
            final_com_results[frame_index].append([info, feature, id, score, det_frame])

            sdly_logger.info("result merge done")

            final_com_results = [x for x in final_com_results if len(x)>2]
            result["result_pic_num"] = len(final_com_results)
            result["result_info"] = []

            result_info = []
            for idx, final_result in enumerate(final_com_results):
                result_info.append({"people_num" : len(final_result)-2,
                                    # "frame" : final_result[0].tobytes(), # encode
                                    "frame" : img2base64(final_result[0]), # encode
                                    "frame_info": []})
                frame_path = os.path.join(file_url, time_id[8:]+"_"+str(idx)+".png")
                cv2.imwrite(frame_path, final_result[0])
                frame_info = []
                for frame_result in final_result[2:]:
                    if frame_result[3]>=recog_threshold:
                        frame_info.append({"face_info": frame_result[0],
                                           "feature": frame_result[1],
                                           "picked_id": frame_result[2],
                                           "det_frame": img2base64(frame_result[-1]), # encode
                                           "exist": True})
                    else:
                        frame_info.append({"face_info": frame_result[0],
                                           "feature": frame_result[1],
                                           "picked_id": "None",
                                           "det_frame": img2base64(frame_result[-1]), # encode
                                           "exist": False})
                result_info[-1]["frame_info"] = frame_info
            result["result_info"]=result_info
        except:
            traceback.print_exc()
            sdly_logger.info("compare Error")
            return self.success("compare Error")
        return self.success(result)

from FaceCheckIn.sys_init import cluster
class DetectVideoAPI(APIView):
    # 人脸特征提取API
    def post(self, request):
        # print(request)
        # print(request.FILES.getlist("files"))
        req_file = request.FILES.get("file")
        detect_threshold = float(request.data["detect_threshold"])
        internal = int(request.data["internal"])

        sdly_logger.info("detect_threshold %f" % detect_threshold)
        sdly_logger.info("internal %d" % internal)

        time_id = tid_maker()
        file_url = os.path.join(sdly_video_root, time_id[:8])

        # 创建年月日文件夹
        if not os.path.exists(file_url):
            try:
                os.mkdir(file_url)
            except:
                sdly_logger.info("服务器空间不够!请联系管理员!")
                return self.success("服务器空间不够!请联系管理员!")
        destination_path = os.path.join(file_url, time_id[8:]+".mp4")
        destination = open(destination_path, 'wb+')
        for chunk in req_file.chunks():
            destination.write(chunk)
        destination.close()
        sdly_logger.info("get vedio data from sdly")
        video_reader = cv2.VideoCapture(destination_path)
        index = 0
        frame_list = []
        while True:
            success, frame = video_reader.read()
            # frame = cv2.transpose(frame)
            if not success:
                sdly_logger.info("video read done")
                break
            if index%internal==0:
                frame_list.append(frame)
            #         frame_list.append(frame)
            index += 1
        sdly_logger.info("get %d pics" % len(frame_list))
        result = {"get_pic_num": len(frame_list)}
        final_com_results = []

        dets_list = detector_object.detect_video(frame_list, threshold=detect_threshold)
        extractor_list = []
        for dets, frame in zip(dets_list, frame_list):
            final_com_results.append([frame])
            final_com_results[-1].append([])
            for det in dets:
                landmark = det[5:15]
                src = np.zeros(10)  # [x1,y1,...]=>[x1,x2...y1,y2...]
                for i in range(5):
                    src[i] = landmark[i*2]
                for i in range(5):
                    src[i+5] = landmark[i*2+1]
                M = findNonreflectiveSimilarity(src, STD)
                cv_res = cv2.warpAffine(frame, M, (int(UNIT_SIZE), int(UNIT_SIZE)))
                pil_res = Image.fromarray(cv2.cvtColor(cv_res, cv2.COLOR_BGR2RGB))
                final_com_results[-1][1].append(cv_res)
                extractor_list.append(pil_res)

        sdly_logger.info("video detect done")
        if len(extractor_list)==0:
            result["result_pic_num"] = 0
            result["result_info"] = []
            return self.success(result)
        try:
            features = extractor_object.extractDeepFeatures(extractor_list)
            # print(feature)
        except:
            traceback.print_exc()
            sdly_logger.info("extract error!")
            return self.success("Internet Error")
        # print(feature)
        sdly_logger.info("video extract done")
        try:
            compare_result = cluster(features, threshold=0.6)
            # sdly_logger.info(str(compare_result))
            feature_index = 0
            for frame_index, dets in enumerate(dets_list):
                for det_index, det in enumerate(dets):
                    compare_result[feature_index]=[compare_result[feature_index], det[4]]
                    compare_result[feature_index].append(frame_index)
                    compare_result[feature_index].append(det_index)
                    compare_result[feature_index].append(feature_index)
                    feature_index+=1

            result_sorted_score = sorted(compare_result, key=lambda x:x[1], reverse=True)
            result_sorted_key = sorted(result_sorted_score, key=lambda x:x[0])

            old_result = result_sorted_key[0]
            for singleresult in result_sorted_key[1:]:
                old_key = old_result[0]
                new_key = singleresult[0]
                if new_key!=old_key:
                    frame_index = old_result[2]
                    det_index = old_result[3]
                    features_index = old_result[4]
                    info = dets_list[frame_index][det_index]
                    det_frame = final_com_results[frame_index][1][det_index]
                    feature = features[features_index]
                    score = old_result[1]
                    id = old_result[0]
                    # if score>=threshold:
                    final_com_results[frame_index].append([info, feature, id, score, det_frame])
                    old_result = singleresult
                else:
                    continue

            frame_index = old_result[2]
            det_index = old_result[3]
            features_index = old_result[4]
            info = dets_list[frame_index][det_index]
            det_frame = final_com_results[frame_index][1][det_index]
            feature = features[features_index]
            score = old_result[1]
            id = old_result[0]
            final_com_results[frame_index].append([info, feature, id, score, det_frame])

            sdly_logger.info("result merge done")

            final_com_results = [x for x in final_com_results if len(x)>2]
            result["result_pic_num"] = len(final_com_results)
            result["result_info"] = []

            result_info = []
            for idx, final_result in enumerate(final_com_results):
                result_info.append({"people_num" : len(final_result)-2,
                                    "frame" : img2base64(final_result[0]), # encode
                                    "frame_info": []})
                frame_path = os.path.join(file_url, time_id[8:]+"_"+str(idx)+".png")
                cv2.imwrite(frame_path, final_result[0])
                frame_info = []
                for frame_result in final_result[2:]:
                    frame_info.append({
                                        "face_score": frame_result[0][4],
                                       "det_frame": img2base64(frame_result[-1]), # encode
                                       })
                    # cv2.imwrite(frame_path, frame_result[-1])

                result_info[-1]["frame_info"] = frame_info
            result["result_info"]=result_info
        except:
            traceback.print_exc()
            sdly_logger.info("compare Error")
            return self.success("compare Error")
        return self.success(result)


class DetectAPI(APIView):
    def post(self, request):
        req_files = request.FILES.getlist("files")
        threshold = float(request.data["threshold"])
        sdly_logger.info("threshold %f"%( threshold ))
        img_list = []
        frame_list = []
        for req_file in req_files:
            time_id = tid_maker()
            temp_file_name = os.path.join(sdly_register_pic_db, time_id + '.png')
            pil_img = Image.open(req_file).convert("RGB")
            pil_img.save(temp_file_name)
            cv_img = cv2.cvtColor(np.asarray(pil_img), cv2.COLOR_RGB2BGR)
            frame_list.append(cv_img)
            sdly_logger.info("get data from sdly")
            img_list.append(pil_img)
        dets_list = detector_object.detect_video(frame_list, threshold=threshold)
        final_com_results = []
        for dets, frame in zip(dets_list, frame_list):
            final_com_results.append([frame])
            # final_com_results[-1].append([])
            for det in dets:
                score = det[4]
                landmark = det[5:15]
                src = np.zeros(10)  # [x1,y1,...]=>[x1,x2...y1,y2...]
                for i in range(5):
                    src[i] = landmark[i*2]
                for i in range(5):
                    src[i+5] = landmark[i*2+1]
                M = findNonreflectiveSimilarity(src, STD)
                cv_res = cv2.warpAffine(frame, M, (int(UNIT_SIZE), int(UNIT_SIZE)))
                final_com_results[-1].append([cv_res, score])

        result=[]
        for frame_result in final_com_results:
            if len(frame_result)==1:
                frame_info={"exists":False, "face_num":0, "info":[]}
            else:
                frame_info={"exists":True, "face_num":len(frame_result)-1, "info":[]}
                for det_result in frame_result[1:]:
                    det_info = {"det_frame":img2base64(det_result[0]), "score":det_result[1]}
                    frame_info["info"].append(det_info)
            result.append(frame_info)

        return self.success(result)


class CheckIn_API(APIView):
    # 人脸签到API-优化版: 显示检测到的人数
    def post(self, request):

        data = request.FILES.get('file').read()
        place = request.data["place"]
        ip = request.data["local_ip"]

        time_id = tid_maker()
        file_url = os.path.join(check_pic_record, time_id[:8])

        # 创建年月日文件夹
        if not os.path.exists(file_url):
            try:
                os.mkdir(file_url)
            except:
                logger.info("服务器空间不够!请联系管理员!")
                return self.success("服务器空间不够!请联系管理员!")
        result = {}
        pil_img = Image.open(request.FILES.get('file'))
        cv_img = cv2.cvtColor(np.asarray(pil_img), cv2.COLOR_RGB2BGR)

        try:
            dets = detector_object.detect(cv_img)
        except:
            traceback.print_exc()
            logger.info("detect error!")
            return self.success("Internet Error")

        result["result"] = [str(dets.shape[0])]
        list_dets = list(dets)
        list_dets.sort(key=lambda x: x[4], reverse=True)

        for det_index in range(len(list_dets)):
            landmark = list_dets[det_index][5:15]
            src = np.zeros(10) # [x1,y1,...]=>[x1,x2...y1,y2...]
            for i in range(5):
                src[i] = landmark[i*2]
            for i in range(5):
                src[i+5] = landmark[i*2+1]
            M = findNonreflectiveSimilarity(src, STD)
            res = cv2.warpAffine(cv_img, M, (int(UNIT_SIZE), int(UNIT_SIZE)))
            pil_res = Image.fromarray(cv2.cvtColor(res, cv2.COLOR_BGR2RGB))
            # pil_res.save("test.png")
            try:
                # with extractor_object as eob:
                #     print("start extract")
                #     feature = eob.extractDeepFeature(pil_res)
                #     print("extract done")
                feature = extractor_object.extractDeepFeature(pil_res)
                feature = feature/cv2.norm(feature)
            except:
                traceback.print_exc()
                logger.info("extract error!")
                return self.success("Internet Error")

            try:
                com_results = face_db.compare(np.array([feature]))
            except:
                traceback.print_exc()
                logger.info("compare error!")
                self.success("please contact manager: db error!")
            for com_result in com_results[0]:
                result["result"].append(com_result[0])
                result["result"].append(com_result[1])

        logger.info(ip)
        message = ""
        for i in range(len(result["result"])):
            if i % 2 != 0:
                message += result["result"][i]+" "
            else:
                message += str(result["result"][i])+" "
        logger.info(message)

        if result["result"][0] == "0":
            file_url = file_url + "/no face"
            # result["tag"]="未检测到人脸"
            if not os.path.exists(file_url):
                os.mkdir(file_url)

            check_face_url = file_url + "/" + time_id[8:-1] + '.png'
            with open(check_face_url, 'wb') as back:
                try:
                    back.write(data)
                    back.close()
                except:
                    logger.info("服务器空间不够!请联系管理员!")
                    return self.success("服务器空间不够!请联系管理员!")
        elif float(result["result"][2]) <= 0.40:
            file_url = file_url + "/can not recognize"
            if not os.path.exists(file_url):
                os.mkdir(file_url)

            check_face_url = file_url + "/" + time_id[8:-1] + '.png'
            with open(check_face_url, 'wb') as back:
                try:
                    back.write(data)
                    back.close()
                except:
                    logger.info("服务器空间不够!请联系管理员!")
                    return self.success("服务器空间不够!请联系管理员!")
        else:
            if int(result["result"][0]) > 1:
                file_url = file_url + "/too many people"
            else:
                file_url = file_url + "/" + result["result"][-2]

            try:
                if not os.path.exists(file_url):
                    os.mkdir(file_url)
            except:
                logger.info("文件夹创建失败!: "+file_url)
                return self.success("服务器空间已经不足!请联系管理员!")

            check_face_url = file_url + "/" + time_id[8:-1] + '.png'

            try:
                back = open(check_face_url, 'wb')
                back.write(data)
                back.close()
            except:
                logger.info("存储照片失败!: "+check_face_url)
                return self.success("服务器空间不足!请联系管理员!")
            try:
                # 查找检测到的人脸是否属于注册人脸
                employees = Employees.objects.filter(username=result["result"][1].split('-')[0]).all()
                if len(employees)==0:
                    result["result"][0] = "-1"
                    return self.success(result)
                inIplist=False
                for employee in employees:
                    employer_id = employee.superior
                    # 查找签到地点的IP是否在白名单内
                    iplist = []
                    ip_records = IPWhiteList.objects.filter(MasterID=employer_id)
                    for ips in ip_records:
                        iplist.append(ips.ip)
                    if ip in iplist:
                        inIplist = True
                        break
                if inIplist:
                    # 查询签到时间信息
                    # checktime_record = TimeSetting.objects.filter(place=place, MasterID=employer_id).first()
                    checktime_record = IPWhiteList.objects.filter(ip=ip).first()

                    current_time = time.localtime(time.time())
                    current = current_time.tm_hour * 60 + current_time.tm_min
                    # 获取最近一次的签到记录
                    logger.info("正在查询最近一次有效的签到记录!")
                    current_record = Records.objects.filter(person_username=employee.username,
                                                            person_real_name=employee.real_name,
                                                            person_superior=employee.superior, is_effective=True).last()

                    logger.info("签到记录查询完毕!")
                    # 已限制打卡时间且开启功能
                    if checktime_record and checktime_record.time_is_active:
                        officetime = checktime_record.OfficeTime
                        officetime_shift = officetime.hour * 60 + officetime.minute
                        offtime = checktime_record.OffTime
                        offtime_shift = offtime.hour * 60 + offtime.minute
                        # log.write("获取打卡时间限制!\n")
                        # 时效内只能签到一次,否则重复签到,记录无效
                        if (current <= officetime_shift and
                            (not current_record or
                             current_record.time.month != current_time.tm_mon or
                             (current_record.time.day < current_time.tm_mday))) \
                                or (current >= offtime_shift and
                                    (not current_record or
                                     current_record.time.month != current_time.tm_mon or
                                     current_record.time.day < current_time.tm_mday or
                                     (current - current_record.time.hour * 60 - current_record.time.minute) >= (
                                             offtime_shift - officetime_shift))):
                            logger.info("在签到规则内!")
                            record = Records.objects.create(img_url=(
                                        check_face_url.split('/')[-3] + "/" + check_face_url.split('/')[-2] + "/" +
                                        check_face_url.split('/')[-1])
                                                            , person_username=employee.username, person_place=place,
                                                            person_real_name=employee.real_name,
                                                            person_superior=employee.superior)
                            record.save()
                            result["tag"] = "签到成功"
                            logger.info("签到成功!")
                        else:  # 重复签到 和 时效范围外签到 均为无效签到
                            logger.info("重复签到/不在签到时间范围内!")
                            record = Records.objects.create(img_url=(
                                    check_face_url.split('/')[-3] + "/" + check_face_url.split('/')[-2] + "/" +
                                    check_face_url.split('/')[-1])
                                , person_username=employee.username, person_place=place,
                                person_real_name=employee.real_name,
                                person_superior=employee.superior, is_effective=False)
                            record.save()
                            result["tag"] = "重复签到/不在签到时间范围内"
                            logger.info("记录完毕!")
                    else:  # 未开启限制功能
                        # checktime_record = TimeSetting.objects.filter(place=place, MasterID=employer_id).first()
                        if (not current_record or
                                current_record.time.month != current_time.tm_mon or
                                (current_record.time.day < current_time.tm_mday)):
                            record = Records.objects.create(img_url=(
                                    check_face_url.split('/')[-3] + "/" + check_face_url.split('/')[-2] + "/" +
                                    check_face_url.split('/')[-1])
                                , person_username=employee.username, person_place=place,
                                person_real_name=employee.real_name,
                                person_superior=employee.superior)
                            result["tag"] = "签到成功（未限制）"
                            record.save()
                            logger.info("签到成功（未限制）")
                        else:
                            record = Records.objects.create(img_url=(
                                    check_face_url.split('/')[-3] + "/" + check_face_url.split('/')[-2] + "/" +
                                    check_face_url.split('/')[-1])
                                , person_username=employee.username, person_place=place,
                                person_real_name=employee.real_name,
                                person_superior=employee.superior, is_effective=False)
                            result["tag"] = "重复签到（未限制）"
                            record.save()
                            logger.info("重复签到（未限制）")
                else:
                    logger.info(str(ip in iplist))
                    result["tag"] = "无法签到\n不在IP白名单内"
            except:
                logger.info("something error!")
        return self.success(result)


class GetAllAPI(APIView):
# 获取签到记录API
    def post(self, request):
        try:
            super_username = request.data["username"]
        except:
            super_username = request.data.dict()["_content"]["username"]
        employer = Employer.objects.filter(username=super_username).first()
        if employer:
            output = []
            output.append({"real_name": employer.real_name, "identification": employer.identification})
            records = Records.objects.filter(person_superior=super_username)

            for i in range(len(records)):
                if records[i].is_effective:
                    temp = {}
                    temp["time"] = records[i].time
                    temp["username"] = records[i].person_username
                    temp["name"] = records[i].person_real_name
                    temp["place"] = records[i].person_place
                    output.append(temp)
            return self.success(output)
        else:
            return self.error("查询失败")

